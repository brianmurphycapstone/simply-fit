const { getJestProjects } = require('@nrwl/jest');

module.exports = {
    projects: getJestProjects(),
    collectCoverage: true,
    collectCoverageFrom: [
        '**/app/**/*.ts',
        '!**/app/**/*module.ts',
        '!**/node_modules/**',
        '!**/environments/**',
    ],
    coverageThreshold: {
        global: {
            branches: 80,
            functions: 80,
            lines: 80,
            statements: 80,
        },
    },
};
