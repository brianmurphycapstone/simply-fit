import { ExercisesService } from './exercises.service';
import { ExerciseResponse } from './../../../../models/exercise';
import { Controller, Get, Param, Post, Req } from '@nestjs/common';

@Controller('exercises')
export class ExercisesController {
    constructor(private readonly exercisesService: ExercisesService) {}

    @Get()
    async getAllExercises(): Promise<ExerciseResponse> {
        return { exercises: await this.exercisesService.getAllExercises() };
    }

    @Get('equipment/:equipment')
    async getExercisesByEquipment(
        @Param('equipment') equipment
    ): Promise<ExerciseResponse> {
        return {
            exercises: await this.exercisesService.getExercisesByEquipment(
                equipment
            ),
        };
    }

    @Get('bodypart/:bodypart')
    async getExercisesByBodypart(
        @Param('bodypart') bodypart
    ): Promise<ExerciseResponse> {
        return {
            exercises: await this.exercisesService.getExercisesByBodypart(
                bodypart
            ),
        };
    }

    @Get('target/:target')
    async getExercisesByTarget(
        @Param('target') target
    ): Promise<ExerciseResponse> {
        return {
            exercises: await this.exercisesService.getExercisesByTarget(target),
        };
    }

    @Post('popular')
    async getPopularExercises(@Req() request): Promise<ExerciseResponse> {
        return {
            exercises: await this.exercisesService.getPopularExercises(
                request.body['email']
            ),
        };
    }
}
