import { Exercise } from './../../../../models/exercise';
import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { DynamoService } from '../dynamo/dynamo.service';

@Injectable()
export class ExercisesService {
    private readonly exerciseDbOptions;

    constructor(
        private httpService: HttpService,
        private dynamoService: DynamoService
    ) {
        this.exerciseDbOptions = {
            headers: {
                'x-rapidapi-host': 'exercisedb.p.rapidapi.com',
                'x-rapidapi-key':
                    '7d383eaf34mshfd2ccd38a6fb44bp1e7c04jsn16f84479936b',
            },
        };
    }

    async getAllExercises(): Promise<Exercise[]> {
        return this.httpService
            .get<Exercise[]>(
                'https://exercisedb.p.rapidapi.com/exercises',
                this.exerciseDbOptions
            )
            .pipe(map((response) => response.data))
            .toPromise();
    }

    async getExercisesByEquipment(equipment: string): Promise<Exercise[]> {
        return this.httpService
            .get<Exercise[]>(
                'https://exercisedb.p.rapidapi.com/exercises/equipment/' +
                    equipment,
                this.exerciseDbOptions
            )
            .pipe(map((response) => response.data))
            .toPromise();
    }

    async getExercisesByBodypart(bodypart: string): Promise<Exercise[]> {
        return this.httpService
            .get<Exercise[]>(
                'https://exercisedb.p.rapidapi.com/exercises/bodyPart/' +
                    bodypart,
                this.exerciseDbOptions
            )
            .pipe(map((response) => response.data))
            .toPromise();
    }

    async getExercisesByTarget(target: string): Promise<Exercise[]> {
        return this.httpService
            .get<Exercise[]>(
                'https://exercisedb.p.rapidapi.com/exercises/target/' + target,
                this.exerciseDbOptions
            )
            .pipe(map((response) => response.data))
            .toPromise();
    }

    async getPopularExercises(email: string): Promise<Exercise[]> {
        return this.dynamoService.getPopularExercises(email);
    }
}
