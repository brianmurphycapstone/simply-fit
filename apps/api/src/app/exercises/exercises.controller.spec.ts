import { ExerciseResponse } from './../../../../models/exercise';
import { ExercisesService } from './exercises.service';
import { Test, TestingModule } from '@nestjs/testing';
import { ExercisesController } from './exercises.controller';
import * as httpMocks from 'node-mocks-http';

const mockExercisesService = {
    getExercisesByEquipment: jest.fn(),
    getExercisesByBodypart: jest.fn(),
    getExercisesByTarget: jest.fn(),
    getAllExercises: jest.fn(),
    getPopularExercises: jest.fn(),
};
describe('ExercisesController', () => {
    let exerciseController: ExercisesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ExercisesController],
            providers: [
                { provide: ExercisesService, useValue: mockExercisesService },
            ],
        }).compile();

        exerciseController =
            module.get<ExercisesController>(ExercisesController);
    });

    it('should be defined', () => {
        expect(exerciseController).toBeDefined();
    });

    describe('Unit: getAllExercises', () => {
        describe('When a request is made to get all exercises', () => {
            let response: ExerciseResponse;

            beforeEach(async () => {
                mockExercisesService.getAllExercises.mockReturnValue([
                    { name: 'exercise' },
                ]);
                response = await exerciseController.getAllExercises();
            });

            it('Should call the exercises service to get all exercises', () => {
                expect(mockExercisesService.getAllExercises).toHaveBeenCalled();
            });

            it('Should return an Exercise Response', () => {
                expect(response).toEqual({ exercises: [{ name: 'exercise' }] });
            });
        });
    });

    describe('Unit: getExercisesByEquipment', () => {
        describe('When a request is made to get exercises by equipment', () => {
            let response: ExerciseResponse;

            beforeEach(async () => {
                mockExercisesService.getExercisesByEquipment.mockReturnValue([
                    { name: 'barbellExercise' },
                ]);
                response = await exerciseController.getExercisesByEquipment(
                    'barbell'
                );
            });

            it('Should call the exercises service with the equipment', () => {
                expect(
                    mockExercisesService.getExercisesByEquipment
                ).toHaveBeenCalledWith('barbell');
            });

            it('Should return an Exercise Response', () => {
                expect(response).toEqual({
                    exercises: [{ name: 'barbellExercise' }],
                });
            });
        });
    });

    describe('Unit: getExercisesByBodypart', () => {
        describe('When a request is made to get exercises by bodypart', () => {
            let response: ExerciseResponse;

            beforeEach(async () => {
                mockExercisesService.getExercisesByBodypart.mockReturnValue([
                    { name: 'backExercise' },
                ]);
                response = await exerciseController.getExercisesByBodypart(
                    'back'
                );
            });

            it('Should call the exercises service with the equipment', () => {
                expect(
                    mockExercisesService.getExercisesByBodypart
                ).toHaveBeenCalledWith('back');
            });

            it('Should return an Exercise Response', () => {
                expect(response).toEqual({
                    exercises: [{ name: 'backExercise' }],
                });
            });
        });
    });

    describe('Unit: getExercisesByTarget', () => {
        describe('When a request is made to get exercises by target', () => {
            let response: ExerciseResponse;

            beforeEach(async () => {
                mockExercisesService.getExercisesByTarget.mockReturnValue([
                    { name: 'abExercise' },
                ]);
                response = await exerciseController.getExercisesByTarget('abs');
            });

            it('Should call the exercises service with the target muscle', () => {
                expect(
                    mockExercisesService.getExercisesByTarget
                ).toHaveBeenCalledWith('abs');
            });

            it('Should return an Exercise Response', () => {
                expect(response).toEqual({
                    exercises: [{ name: 'abExercise' }],
                });
            });
        });
    });

    describe('Unit: getPopularExercises', () => {
        describe('Given a request with an email', () => {
            let request;

            beforeEach(() => {
                request = httpMocks.createRequest({
                    body: {
                        email: 'email',
                    },
                });
            });

            describe('When a request is made to get popular exercises', () => {
                let response: ExerciseResponse;

                beforeEach(async () => {
                    mockExercisesService.getPopularExercises.mockReturnValue([
                        { name: 'exercise' },
                    ]);
                    response = await exerciseController.getPopularExercises(
                        request
                    );
                });

                it('Should call the exercises service to get popular exercises', () => {
                    expect(
                        mockExercisesService.getPopularExercises
                    ).toHaveBeenCalled();
                });

                it('Should return an Exercise Response', () => {
                    expect(response).toEqual({
                        exercises: [{ name: 'exercise' }],
                    });
                });
            });
        });
    });
});
