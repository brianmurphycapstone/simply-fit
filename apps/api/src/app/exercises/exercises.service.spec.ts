import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Exercise } from '../../../../models/exercise';
import { of } from 'rxjs';
import { ExercisesService } from './exercises.service';
import { DynamoService } from '../dynamo/dynamo.service';

const mockHttpService = {
    get: jest.fn(),
};

const mockDynamoService = {
    getPopularExercises: jest.fn(),
};

describe('ExercisesService', () => {
    let exerciseService: ExercisesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                ExercisesService,
                { provide: HttpService, useValue: mockHttpService },
                { provide: DynamoService, useValue: mockDynamoService },
            ],
        }).compile();

        exerciseService = module.get<ExercisesService>(ExercisesService);
    });

    it('should be defined', () => {
        expect(exerciseService).toBeDefined();
    });

    it('Should set the exerciseDbOptions', () => {
        expect(exerciseService['exerciseDbOptions']).toEqual({
            headers: {
                'x-rapidapi-host': 'exercisedb.p.rapidapi.com',
                'x-rapidapi-key':
                    '7d383eaf34mshfd2ccd38a6fb44bp1e7c04jsn16f84479936b',
            },
        });
    });

    describe('Unit: getAllExercises', () => {
        describe('When a request is made to get all exercises', () => {
            let response: Exercise[];

            beforeEach(async () => {
                mockHttpService.get.mockReturnValue(
                    of({ data: [{ exercise1: {} }, { exercise2: {} }] })
                );
                response = await exerciseService.getAllExercises();
            });

            it('Should make a request to get exercises from the exercise DB', () => {
                expect(mockHttpService.get).toHaveBeenCalledWith(
                    'https://exercisedb.p.rapidapi.com/exercises',
                    exerciseService['exerciseDbOptions']
                );
            });

            it('Should return a list of exercises', () => {
                expect(response).toEqual([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
            });
        });
    });

    describe('Unit: getExercisesByEquipment', () => {
        describe('When a request is made to get exercises by equipment', () => {
            let response: Exercise[];

            beforeEach(async () => {
                mockHttpService.get.mockReturnValue(
                    of({ data: [{ exercise1: {} }, { exercise2: {} }] })
                );
                response = await exerciseService.getExercisesByEquipment(
                    'barbell'
                );
            });

            it('Should make a request to get exercises from the exercise DB', () => {
                expect(mockHttpService.get).toHaveBeenCalledWith(
                    'https://exercisedb.p.rapidapi.com/exercises/equipment/barbell',
                    exerciseService['exerciseDbOptions']
                );
            });

            it('Should return a list of exercises', () => {
                expect(response).toEqual([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
            });
        });
    });

    describe('Unit: getExercisesByBodypart', () => {
        describe('When a request is made to get exercises by bodypart', () => {
            let response: Exercise[];

            beforeEach(async () => {
                mockHttpService.get.mockReturnValue(
                    of({ data: [{ exercise1: {} }, { exercise2: {} }] })
                );
                response = await exerciseService.getExercisesByBodypart('back');
            });

            it('Should make a request to get exercises from the exercise DB', () => {
                expect(mockHttpService.get).toHaveBeenCalledWith(
                    'https://exercisedb.p.rapidapi.com/exercises/bodyPart/back',
                    exerciseService['exerciseDbOptions']
                );
            });

            it('Should return a list of exercises', () => {
                expect(response).toEqual([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
            });
        });
    });

    describe('Unit: getExercisesByTarget', () => {
        describe('When a request is made to get exercises by target', () => {
            let response: Exercise[];

            beforeEach(async () => {
                mockHttpService.get.mockReturnValue(
                    of({ data: [{ exercise1: {} }, { exercise2: {} }] })
                );
                response = await exerciseService.getExercisesByTarget('abs');
            });

            it('Should make a request to get exercises from the exercise DB', () => {
                expect(mockHttpService.get).toHaveBeenCalledWith(
                    'https://exercisedb.p.rapidapi.com/exercises/target/abs',
                    exerciseService['exerciseDbOptions']
                );
            });

            it('Should return a list of exercises', () => {
                expect(response).toEqual([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
            });
        });
    });

    describe('Unit: getPopularExercises', () => {
        describe('When a request is made to get popular exercises', () => {
            let response: Exercise[];

            beforeEach(async () => {
                mockDynamoService.getPopularExercises.mockResolvedValue([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
                response = await exerciseService.getPopularExercises('email');
            });

            it('Should make a request to get popular exercises from the dynamo service', () => {
                expect(
                    mockDynamoService.getPopularExercises
                ).toHaveBeenCalledWith('email');
            });

            it('Should return a list of exercises', () => {
                expect(response).toEqual([
                    { exercise1: {} },
                    { exercise2: {} },
                ]);
            });
        });
    });
});
