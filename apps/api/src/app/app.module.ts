import { AccountService } from './account/account.service';
import { AccountController } from './account/account.controller';
import { HttpModule, Module } from '@nestjs/common';

import { DynamoService } from './dynamo/dynamo.service';
import { LoginService } from './login/login.service';
import { LoginController } from './login/login.controller';
import { ExercisesService } from './exercises/exercises.service';
import { ExercisesController } from './exercises/exercises.controller';
import { WorkoutController } from './workout/workout.controller';
import { WorkoutService } from './workout/workout.service';

@Module({
    imports: [HttpModule],
    controllers: [
        AccountController,
        LoginController,
        ExercisesController,
        WorkoutController,
    ],
    providers: [
        AccountService,
        DynamoService,
        LoginService,
        ExercisesService,
        WorkoutService,
    ],
})
export class AppModule {}
