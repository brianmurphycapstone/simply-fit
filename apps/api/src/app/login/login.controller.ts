import { LoginResponse } from './../../../../models/login';
import { Controller, Post, Req, Res } from '@nestjs/common';
import { Account } from './../../../../models/account';
import { LoginService } from './login.service';

@Controller()
export class LoginController {
    constructor(private readonly loginService: LoginService) {}

    @Post('login')
    async login(
        @Req() request,
        @Res({ passthrough: true }) response
    ): Promise<LoginResponse> {
        const account: Account = {
            email: request.body['email'],
            password: request.body['password'],
        };

        const credentialsValid = await this.loginService.login(account);
        if (credentialsValid) {
            response['cookie']('loggedInAccount', account.email);
        }
        return { credentialsValid: credentialsValid };
    }
}
