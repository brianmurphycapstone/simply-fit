import { LoginResponse } from './../../../../models/login';
import { Test, TestingModule } from '@nestjs/testing';
import { LoginController } from './login.controller';
import { LoginService } from './login.service';
import * as httpMocks from 'node-mocks-http';

const mockLoginService = {
    login: jest.fn(),
};

describe('LoginController', () => {
    let loginController: LoginController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [LoginController],
            providers: [{ provide: LoginService, useValue: mockLoginService }],
        }).compile();

        loginController = module.get<LoginController>(LoginController);
    });

    it('should be defined', () => {
        expect(loginController).toBeDefined();
    });

    describe('Unit: login', () => {
        describe('Given a request with an account', () => {
            let request;
            let response;
            let responseCookieSpy;

            beforeEach(() => {
                request = httpMocks.createRequest({
                    body: {
                        email: 'email',
                        password: 'password',
                    },
                });

                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                response = httpMocks.createResponse();
                responseCookieSpy = jest.spyOn(response, 'cookie');
            });

            describe('And credentials are valid', () => {
                beforeEach(() => {
                    mockLoginService.login.mockReturnValue(true);
                });
                describe('When a request to login is made', () => {
                    let loginResponse: LoginResponse;

                    beforeEach(async () => {
                        loginResponse = await loginController.login(
                            request,
                            response
                        );
                    });

                    it('should call the login service', () => {
                        expect(mockLoginService.login).toHaveBeenCalled();
                    });

                    it('should set the reponse cookie', () => {
                        expect(responseCookieSpy).toHaveBeenCalled();
                    });

                    it('should return a login response with credentials valid', () => {
                        expect(loginResponse).toEqual({
                            credentialsValid: true,
                        });
                    });
                });
            });

            describe('And credentials are NOT valid', () => {
                beforeEach(() => {
                    mockLoginService.login.mockReturnValue(false);
                });
                describe('When a request to login is made', () => {
                    let loginResponse: LoginResponse;

                    beforeEach(async () => {
                        loginResponse = await loginController.login(
                            request,
                            response
                        );
                    });

                    it('should call the login service', () => {
                        expect(mockLoginService.login).toHaveBeenCalled();
                    });

                    it('should NOT set the reponse cookie', () => {
                        expect(responseCookieSpy).not.toHaveBeenCalled();
                    });

                    it('should return a login response with credentials invalid', () => {
                        expect(loginResponse).toEqual({
                            credentialsValid: false,
                        });
                    });
                });
            });
        });
    });
});
