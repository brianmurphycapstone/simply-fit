import { Injectable } from '@nestjs/common';
import { Account } from '../../../../models/account';
import { DynamoService } from '../dynamo/dynamo.service';

@Injectable()
export class LoginService {
    constructor(private readonly dynamoService: DynamoService) {}

    async login(account: Account): Promise<boolean> {
        const accountRecord = await this.dynamoService.getAccount(
            account.email
        );
        if (accountRecord && accountRecord.password === account.password) {
            return true;
        } else {
            return false;
        }
    }
}
