import { Test, TestingModule } from '@nestjs/testing';
import { Account } from '../../../../models/account';
import { DynamoService } from '../dynamo/dynamo.service';
import { LoginService } from './login.service';

const mockDynamoService = {
    getAccount: jest.fn(),
};

describe('LoginService', () => {
    let loginService: LoginService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                LoginService,
                { provide: DynamoService, useValue: mockDynamoService },
            ],
        }).compile();

        loginService = module.get<LoginService>(LoginService);
    });

    it('should be defined', () => {
        expect(loginService).toBeDefined();
    });

    describe('Unit: login', () => {
        describe('Given an account', () => {
            let account: Account;

            beforeEach(() => {
                account = {
                    email: 'email',
                    password: 'password',
                };
            });

            describe('And the account exists in Dynamo with a matching password', () => {
                beforeEach(() => {
                    mockDynamoService.getAccount.mockReturnValue(account);
                });

                describe('When making a request to login', () => {
                    let response: boolean;

                    beforeEach(async () => {
                        response = await loginService.login(account);
                    });

                    it('should return login was successful', () => {
                        expect(response).toEqual(true);
                    });
                });
            });

            describe('And the account exists in Dynamo but the password doesnt match', () => {
                beforeEach(() => {
                    mockDynamoService.getAccount.mockReturnValue({
                        email: 'email',
                        password: 'differentPassword',
                    });
                });

                describe('When making a request to login', () => {
                    let response: boolean;

                    beforeEach(async () => {
                        response = await loginService.login(account);
                    });

                    it('should return login was NOT successful', () => {
                        expect(response).toEqual(false);
                    });
                });
            });

            describe('And the account does not exist in Dynamo', () => {
                beforeEach(() => {
                    mockDynamoService.getAccount.mockReturnValue({
                        email: 'email',
                        password: 'differentPassword',
                    });
                });

                describe('When making a request to login', () => {
                    let response: boolean;

                    beforeEach(async () => {
                        response = await loginService.login(account);
                    });

                    it('should return login was NOT successful', () => {
                        expect(response).toEqual(false);
                    });
                });
            });
        });
    });
});
