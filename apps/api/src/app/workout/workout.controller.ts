import {
    SaveWorkoutResponse,
    Workout,
    GetWorkoutResponse,
} from './../../../../models/workout';
import { Controller, Post, Req } from '@nestjs/common';
import { WorkoutService } from './workout.service';

@Controller()
export class WorkoutController {
    constructor(private readonly workoutService: WorkoutService) {}

    @Post('saveWorkout')
    async saveWorkout(@Req() request): Promise<SaveWorkoutResponse> {
        const email = request.body['email'];
        const workout: Workout = {
            name: request.body['workout']['name'],
            exercises: request.body['workout']['exercises'],
        };
        return {
            status: await this.workoutService.saveWorkout(workout, email),
        };
    }

    @Post('getWorkouts')
    async getWorkouts(@Req() request): Promise<GetWorkoutResponse> {
        const email = request.body['email'];
        return {
            workouts: await this.workoutService.getWorkouts(email),
        };
    }
}
