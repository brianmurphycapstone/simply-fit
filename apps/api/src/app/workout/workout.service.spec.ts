import { SaveWorkoutStatus } from './../../../../models/enums';
import { Workout } from './../../../../models/workout';
import { Test, TestingModule } from '@nestjs/testing';
import { DynamoService } from '../dynamo/dynamo.service';
import { WorkoutService } from './workout.service';

const mockDynamoService = {
    getWorkouts: jest.fn(),
    saveWorkout: jest.fn(),
};

jest.spyOn(console, 'log').mockImplementation();

describe('WorkoutService', () => {
    let workoutService: WorkoutService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                WorkoutService,
                { provide: DynamoService, useValue: mockDynamoService },
            ],
        }).compile();

        workoutService = module.get<WorkoutService>(WorkoutService);
    });

    it('should be defined', () => {
        expect(workoutService).toBeDefined();
    });

    describe('Unit: saveWorkout', () => {
        describe('Given a workout and an email', () => {
            let email: string;
            let workout: Workout;

            beforeEach(() => {
                email = 'email';
                workout = {
                    name: 'workoutName',
                    exercises: [],
                };
            });

            describe('And the workout name already exists in Dynamo', () => {
                beforeEach(() => {
                    mockDynamoService.getWorkouts.mockReturnValue([
                        { name: 'workoutName', exercises: [] } as Workout,
                    ]);
                });

                describe('When making a request to save a workout', () => {
                    let response: SaveWorkoutStatus;

                    beforeEach(async () => {
                        response = await workoutService.saveWorkout(
                            workout,
                            email
                        );
                    });

                    it('should return an WORKOUT_ALREADY_EXISTS status', () => {
                        expect(response).toEqual(
                            SaveWorkoutStatus.WORKOUT_ALREADY_EXISTS
                        );
                    });
                });
            });

            describe('And the workout does NOT exist in Dynamo', () => {
                beforeEach(() => {
                    mockDynamoService.getWorkouts.mockReturnValue([]);
                });

                describe('And the workout is saved successfully', () => {
                    beforeEach(() => {
                        mockDynamoService.saveWorkout.mockReturnValue(
                            SaveWorkoutStatus.WORKOUT_SAVED
                        );
                    });

                    describe('When making a request to save a workout', () => {
                        let response: SaveWorkoutStatus;

                        beforeEach(async () => {
                            response = await workoutService.saveWorkout(
                                workout,
                                email
                            );
                        });

                        it('should return that the workout was saved successfully', () => {
                            expect(response).toEqual(
                                SaveWorkoutStatus.WORKOUT_SAVED
                            );
                        });
                    });
                });

                describe('And the workout fails to save', () => {
                    beforeEach(() => {
                        mockDynamoService.saveWorkout.mockReturnValue(
                            SaveWorkoutStatus.WORKOUT_SAVE_FAILED
                        );
                    });

                    describe('When making a request to save a workout', () => {
                        let response: SaveWorkoutStatus;

                        beforeEach(async () => {
                            response = await workoutService.saveWorkout(
                                workout,
                                email
                            );
                        });

                        it('should return that the workout failed to save', () => {
                            expect(response).toEqual(
                                SaveWorkoutStatus.WORKOUT_SAVE_FAILED
                            );
                        });
                    });
                });
            });
        });
    });

    describe('Unit: getWorkouts', () => {
        describe('Given an email', () => {
            let email: string;

            beforeEach(() => {
                email = 'email';
            });

            describe('When retrieving workouts', () => {
                let response;

                beforeEach(async () => {
                    mockDynamoService.getWorkouts.mockReturnValue([
                        { name: 'workoutName', exercises: [] } as Workout,
                    ]);
                    response = await workoutService.getWorkouts(email);
                });

                it('should return the retrieved workouts', () => {
                    expect(response).toEqual([
                        { name: 'workoutName', exercises: [] } as Workout,
                    ]);
                });
            });
        });
    });
});
