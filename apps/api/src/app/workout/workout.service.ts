import { SaveWorkoutStatus } from './../../../../models/enums';
import { Workout } from './../../../../models/workout';
import { Injectable } from '@nestjs/common';
import { DynamoService } from '../dynamo/dynamo.service';

@Injectable()
export class WorkoutService {
    constructor(private readonly dynamoService: DynamoService) {}

    async saveWorkout(
        workout: Workout,
        email: string
    ): Promise<SaveWorkoutStatus> {
        const workouts: Workout[] = await this.dynamoService.getWorkouts(email);
        if (
            workouts.some(
                (retrievedWorkout: Workout) =>
                    workout.name === retrievedWorkout.name
            )
        ) {
            return SaveWorkoutStatus.WORKOUT_ALREADY_EXISTS;
        } else {
            return this.dynamoService.saveWorkout(email, workout);
        }
    }

    async getWorkouts(email: string): Promise<Workout[]> {
        return await this.dynamoService.getWorkouts(email);
    }
}
