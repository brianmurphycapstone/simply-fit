import { SaveWorkoutResponse } from './../../../../models/workout';
import { SaveWorkoutStatus } from './../../../../models/enums';
import { WorkoutService } from './workout.service';
import { Test, TestingModule } from '@nestjs/testing';
import { WorkoutController } from './workout.controller';
import * as httpMocks from 'node-mocks-http';

const mockWorkoutService = {
    saveWorkout: jest.fn(),
    getWorkouts: jest.fn(),
};

describe('WorkoutController', () => {
    let workoutController: WorkoutController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [WorkoutController],
            providers: [
                { provide: WorkoutService, useValue: mockWorkoutService },
            ],
        }).compile();

        workoutController = module.get<WorkoutController>(WorkoutController);
    });

    it('should be defined', () => {
        expect(workoutController).toBeDefined();
    });

    describe('Unit: saveWorkout', () => {
        describe('Given a request with an email and a workout', () => {
            let request;
            beforeEach(() => {
                request = httpMocks.createRequest({
                    body: {
                        email: 'email',
                        workout: {
                            name: 'workoutName',
                            exercises: [],
                        },
                    },
                });
            });

            describe('And the workout is saved successfully', () => {
                beforeEach(() => {
                    mockWorkoutService.saveWorkout.mockReturnValue(
                        SaveWorkoutStatus.WORKOUT_SAVED
                    );
                });

                describe('When a request is made to save a workout', () => {
                    let response: SaveWorkoutResponse;

                    beforeEach(async () => {
                        response = await workoutController.saveWorkout(request);
                    });

                    it('should call the workout service', () => {
                        expect(
                            mockWorkoutService.saveWorkout
                        ).toHaveBeenCalled();
                    });

                    it('should return a save workout response', () => {
                        expect(response).toEqual({
                            status: SaveWorkoutStatus.WORKOUT_SAVED,
                        });
                    });
                });
            });
        });
    });

    describe('Unit: getWorkouts', () => {
        describe('Given a request with an email', () => {
            let request;
            beforeEach(() => {
                request = httpMocks.createRequest({
                    body: {
                        email: 'email',
                    },
                });
            });

            describe('When retrieving workouts', () => {
                let response;

                beforeEach(async () => {
                    mockWorkoutService.getWorkouts.mockReturnValue([]);
                    response = await workoutController.getWorkouts(request);
                });
                it('should retrieve workouts from the workout service', () => {
                    expect(mockWorkoutService.getWorkouts).toHaveBeenCalled();
                    expect(response).toEqual({ workouts: [] });
                });
            });
        });
    });
});
