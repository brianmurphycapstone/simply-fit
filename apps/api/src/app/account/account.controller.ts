import { Controller, Post, Req } from '@nestjs/common';
import { Account, AccountCreationResponse } from '../../../../models/account';
import { AccountService } from './account.service';

@Controller()
export class AccountController {
    constructor(private readonly accountService: AccountService) {}

    @Post('createAccount')
    async createAccount(@Req() request): Promise<AccountCreationResponse> {
        const account: Account = {
            email: request.body['email'],
            password: request.body['password'],
        };
        return { status: await this.accountService.createAccount(account) };
    }
}
