import { AccountCreationStatus } from '../../../../models/enums';
import { AccountController } from './account.controller';
import { Test, TestingModule } from '@nestjs/testing';
import * as httpMocks from 'node-mocks-http';
import { AccountService } from './account.service';
import { AccountCreationResponse } from '../../../../models/account';

const mockAccountService = {
    createAccount: jest.fn(),
};

describe('AccountController', () => {
    let accountController: AccountController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AccountController],
            providers: [
                { provide: AccountService, useValue: mockAccountService },
            ],
        }).compile();

        accountController = module.get<AccountController>(AccountController);
    });

    it('should be defined', () => {
        expect(accountController).toBeDefined();
    });

    describe('Unit: createAccount', () => {
        describe('Given a request with an account', () => {
            let request;

            beforeEach(() => {
                request = httpMocks.createRequest({
                    body: {
                        email: 'email',
                        password: 'password',
                    },
                });
            });

            describe('And the account is created successfully', () => {
                beforeEach(() => {
                    mockAccountService.createAccount.mockReturnValue(
                        AccountCreationStatus.ACCOUNT_CREATED
                    );
                });

                describe('When a request to create an account is made', () => {
                    let response: AccountCreationResponse;

                    beforeEach(async () => {
                        response = await accountController.createAccount(
                            request
                        );
                    });

                    it('should call the account service', () => {
                        expect(
                            mockAccountService.createAccount
                        ).toHaveBeenCalled();
                    });

                    it('should return an account creation response', () => {
                        expect(response).toEqual({
                            status: AccountCreationStatus.ACCOUNT_CREATED,
                        });
                    });
                });
            });
        });
    });
});
