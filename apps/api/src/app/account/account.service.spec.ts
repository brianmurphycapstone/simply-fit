import { AccountService } from './account.service';
import { Test, TestingModule } from '@nestjs/testing';
import { Account } from '../../../../models/account';
import { DynamoService } from '../dynamo/dynamo.service';
import { AccountCreationStatus } from '../../../../models/enums';

const mockDynamoService = {
    getAccount: jest.fn(),
    saveAccount: jest.fn(),
};

jest.spyOn(console, 'log').mockImplementation();

describe('AccountService', () => {
    let accountService: AccountService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AccountService,
                { provide: DynamoService, useValue: mockDynamoService },
            ],
        }).compile();

        accountService = module.get<AccountService>(AccountService);
    });

    it('should be defined', () => {
        expect(accountService).toBeDefined();
    });

    describe('Unit: createAccount', () => {
        describe('Given an account', () => {
            let account: Account;

            beforeEach(() => {
                account = {
                    email: 'email',
                    password: 'password',
                };
            });

            describe('And the account exists in Dynamo', () => {
                beforeEach(() => {
                    mockDynamoService.getAccount.mockReturnValue(account);
                });

                describe('When making a request to create an account', () => {
                    let response: AccountCreationStatus;

                    beforeEach(async () => {
                        response = await accountService.createAccount(account);
                    });

                    it('should return an ACCOUNT_ALREADY_EXISTS status', () => {
                        expect(response).toEqual(
                            AccountCreationStatus.ACCOUNT_ALREADY_EXISTS
                        );
                    });
                });
            });

            describe('And the account does NOT exist in Dynamo', () => {
                beforeEach(() => {
                    mockDynamoService.getAccount.mockReturnValue(null);
                });

                describe('And the account is created successfully', () => {
                    beforeEach(() => {
                        mockDynamoService.saveAccount.mockReturnValue(
                            AccountCreationStatus.ACCOUNT_CREATED
                        );
                    });

                    describe('When making a request to create an account', () => {
                        let response: AccountCreationStatus;

                        beforeEach(async () => {
                            response = await accountService.createAccount(
                                account
                            );
                        });

                        it('should return that the account was created successfully', () => {
                            expect(response).toEqual(
                                AccountCreationStatus.ACCOUNT_CREATED
                            );
                        });
                    });
                });

                describe('And the account creation fails', () => {
                    beforeEach(() => {
                        mockDynamoService.saveAccount.mockReturnValue(
                            AccountCreationStatus.ACCOUNT_CREATION_FAILED
                        );
                    });

                    describe('When making a request to create an account', () => {
                        let response: AccountCreationStatus;

                        beforeEach(async () => {
                            response = await accountService.createAccount(
                                account
                            );
                        });

                        it('should return that the account creation failed', () => {
                            expect(response).toEqual(
                                AccountCreationStatus.ACCOUNT_CREATION_FAILED
                            );
                        });
                    });
                });
            });
        });
    });
});
