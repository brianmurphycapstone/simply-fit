import { Injectable } from '@nestjs/common';
import { Account } from '../../../../models/account';
import { AccountCreationStatus } from '../../../../models/enums';
import { DynamoService } from '../dynamo/dynamo.service';

@Injectable()
export class AccountService {
    constructor(private readonly dynamoService: DynamoService) {}

    async createAccount(account: Account): Promise<AccountCreationStatus> {
        console.log(`email: ${account.email} \n password: ${account.password}`);
        const existingAccount = await this.dynamoService.getAccount(
            account.email
        );
        if (existingAccount) {
            return AccountCreationStatus.ACCOUNT_ALREADY_EXISTS;
        } else {
            return this.dynamoService.saveAccount(account);
        }
    }
}
