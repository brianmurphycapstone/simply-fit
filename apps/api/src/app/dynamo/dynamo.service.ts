import { SaveWorkoutStatus } from './../../../../models/enums';
import { Workout } from './../../../../models/workout';
import { Exercise } from './../../../../models/exercise';
import { Injectable } from '@nestjs/common';
import {
    DynamoDBClient,
    GetItemCommand,
    PutItemCommand,
    UpdateItemCommand,
} from '@aws-sdk/client-dynamodb';
import { AccountCreationStatus } from '../../../../models/enums';
import { Account } from '../../../../models/account';

@Injectable()
export class DynamoService {
    private readonly table;
    private readonly client;

    constructor() {
        this.table = 'SimplyFit';

        this.client = new DynamoDBClient({
            credentials: {
                accessKeyId: 'AKIA4NADUNXMFCEDDHVD',
                secretAccessKey: '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',
            },
            region: 'us-east-1',
        });
    }

    async saveAccount(account: Account): Promise<AccountCreationStatus> {
        const params = {
            TableName: this.table,
            Item: {
                email: { S: account.email },
                password: { S: account.password },
            },
        };
        const data = await this.client.send(new PutItemCommand(params));
        if (data.$metadata.httpStatusCode === 200) {
            console.log('Account Created Successfully');
            return AccountCreationStatus.ACCOUNT_CREATED;
        } else {
            console.error('Account Creation Failed');
            return AccountCreationStatus.ACCOUNT_CREATION_FAILED;
        }
    }

    async getAccount(email: string): Promise<Account> {
        let account: Account = null;
        const params = {
            TableName: this.table,
            Key: {
                email: { S: email },
            },
        };

        const data = await this.client.send(new GetItemCommand(params));
        if (data.Item) {
            account = {
                email: data.Item.email.S,
                password: data.Item.password.S,
            };
        }
        console.log(`Retrieved Account: ${JSON.stringify(account)}`);
        return account;
    }

    async saveWorkout(
        email: string,
        workout: Workout
    ): Promise<SaveWorkoutStatus> {
        const formattedExercises = this.formatExercises(workout.exercises);
        const params = {
            TableName: this.table,
            Key: {
                email: { S: email },
            },
            UpdateExpression: 'SET #workoutName = :exercises',
            ExpressionAttributeNames: {
                '#workoutName': workout.name,
            },
            ExpressionAttributeValues: {
                ':exercises': { L: formattedExercises },
            },
        };
        const data = await this.client.send(new UpdateItemCommand(params));
        if (data.$metadata.httpStatusCode === 200) {
            console.log('Workout Saved Successfully');
            return SaveWorkoutStatus.WORKOUT_SAVED;
        } else {
            console.error('Workout Save Failed');
            return SaveWorkoutStatus.WORKOUT_SAVE_FAILED;
        }
    }

    async getWorkouts(email: string): Promise<Workout[]> {
        let workouts: Workout[] = [];
        const params = {
            TableName: this.table,
            Key: {
                email: { S: email },
            },
        };

        const data = await this.client.send(new GetItemCommand(params));
        if (data.Item) {
            workouts = this.getWorkoutsFromDynamoItem(data.Item);
        }
        return workouts;
    }

    async getPopularExercises(email: string): Promise<Exercise[]> {
        let workouts: Workout[] = [];
        const exercises: Exercise[] = [];
        const params = {
            TableName: this.table,
            Key: {
                email: { S: email },
            },
        };

        const data = await this.client.send(new GetItemCommand(params));
        if (data.Item) {
            workouts = this.getWorkoutsFromDynamoItem(data.Item);
            workouts.forEach((workout) => {
                exercises.push(...workout.exercises);
            });
        }
        return exercises;
    }

    formatExercises(exercises: Exercise[]) {
        const formattedExercises = [];
        exercises.forEach((exercise) => {
            formattedExercises.push({
                M: {
                    bodyPart: { S: `${exercise.bodyPart}` },
                    equipment: { S: `${exercise.equipment}` },
                    gifUrl: { S: `${exercise.gifUrl}` },
                    id: { S: `${exercise.id}` },
                    name: { S: `${exercise.name}` },
                    target: { S: `${exercise.target}` },
                },
            });
        });
        return formattedExercises;
    }

    getWorkoutsFromDynamoItem(item): Workout[] {
        const workouts: Workout[] = [];

        for (const [itemKey, itemValue] of Object.entries(item)) {
            if (itemKey !== 'email' && itemKey !== 'password') {
                const workout = {} as Workout;
                workout.name = itemKey;
                workout.exercises = [];

                itemValue['L'].forEach((itemExercise) => {
                    const exercise = {} as Exercise;

                    for (const [exerciseKey, exerciseValue] of Object.entries(
                        itemExercise['M']
                    )) {
                        exercise[exerciseKey] = exerciseValue['S'];
                    }

                    workout.exercises.push(exercise);
                });

                workouts.push(workout);
            }
        }

        return workouts;
    }
}
