import { SaveWorkoutStatus } from './../../../../models/enums';
import { mockClient } from 'aws-sdk-client-mock';
import { Test, TestingModule } from '@nestjs/testing';
import { Account } from '../../../../models/account';
import { DynamoService } from '../dynamo/dynamo.service';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { AccountCreationStatus } from '../../../../models/enums';
import { Workout } from '../../../../models/workout';
import { Exercise } from '../../../../models/exercise';

const dynamoMock = mockClient(DynamoDBClient);

jest.spyOn(console, 'log').mockImplementation();
jest.spyOn(console, 'error').mockImplementation();

describe('DynamoService', () => {
    let dynamoService: DynamoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DynamoService],
        }).compile();

        dynamoService = module.get<DynamoService>(DynamoService);
        dynamoMock.reset();
    });

    it('should be defined', () => {
        expect(dynamoService).toBeDefined();
    });

    describe('Unit: saveAccount', () => {
        describe('Given an account', () => {
            let account: Account;

            beforeEach(() => {
                account = {
                    email: 'email',
                    password: 'password',
                };
            });

            describe('And the account is successfully saved in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        $metadata: {
                            httpStatusCode: 200,
                        },
                    });
                });
                describe('When saving an account', () => {
                    let response: AccountCreationStatus;
                    beforeEach(async () => {
                        response = await dynamoService.saveAccount(account);
                    });

                    it('should return the account was created successfully', () => {
                        expect(response).toEqual(
                            AccountCreationStatus.ACCOUNT_CREATED
                        );
                    });
                });
            });

            describe('And the account fails to save in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        $metadata: {
                            httpStatusCode: 500,
                        },
                    });
                });
                describe('When saving an account', () => {
                    let response: AccountCreationStatus;
                    beforeEach(async () => {
                        response = await dynamoService.saveAccount(account);
                    });

                    it('should return that account creation failed', () => {
                        expect(response).toEqual(
                            AccountCreationStatus.ACCOUNT_CREATION_FAILED
                        );
                    });
                });
            });
        });
    });

    describe('Unit: getAccount', () => {
        describe('Given an email', () => {
            let email: string;

            beforeEach(() => {
                email = 'email';
            });

            describe('And the email exists in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        Item: {
                            email: {
                                S: 'email',
                            },
                            password: {
                                S: 'password',
                            },
                        },
                    });
                });
                describe('When retrieving an account', () => {
                    let response: Account;
                    beforeEach(async () => {
                        response = await dynamoService.getAccount(email);
                    });

                    it('should return the account', () => {
                        expect(response).toEqual({
                            email: 'email',
                            password: 'password',
                        });
                    });
                });
            });

            describe('And the email does NOT exist in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({});
                });
                describe('When retrieving an account', () => {
                    let response: Account;
                    beforeEach(async () => {
                        response = await dynamoService.getAccount(email);
                    });

                    it('should return the account', () => {
                        expect(response).toEqual(null);
                    });
                });
            });
        });
    });

    describe('Unit: saveWorkout', () => {
        describe('Given a workout and an email', () => {
            let email: string;
            let workout: Workout;

            beforeEach(() => {
                email = 'email';
                workout = {
                    name: 'workoutName',
                    exercises: [],
                };
            });

            describe('And the workout is successfully saved in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        $metadata: {
                            httpStatusCode: 200,
                        },
                    });
                });
                describe('When saving a workout', () => {
                    let response: SaveWorkoutStatus;
                    beforeEach(async () => {
                        response = await dynamoService.saveWorkout(
                            email,
                            workout
                        );
                    });

                    it('should return the workout was saved successfully', () => {
                        expect(response).toEqual(
                            SaveWorkoutStatus.WORKOUT_SAVED
                        );
                    });
                });
            });

            describe('And the workout fails to save in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        $metadata: {
                            httpStatusCode: 500,
                        },
                    });
                });
                describe('When saving a workout', () => {
                    let response: SaveWorkoutStatus;
                    beforeEach(async () => {
                        response = await dynamoService.saveWorkout(
                            email,
                            workout
                        );
                    });

                    it('should return that workout failed to save', () => {
                        expect(response).toEqual(
                            SaveWorkoutStatus.WORKOUT_SAVE_FAILED
                        );
                    });
                });
            });
        });
    });

    describe('Unit: getWorkouts', () => {
        describe('Given an email', () => {
            let email: string;

            beforeEach(() => {
                email = 'email';
            });

            describe('And the email exists in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        Item: {
                            password: {
                                S: 'password',
                            },
                            email: {
                                S: 'test@test.com',
                            },
                            workout1: {
                                L: [
                                    {
                                        M: {
                                            name: {
                                                S: 'alternate lateral pulldown',
                                            },
                                            equipment: {
                                                S: 'cable',
                                            },
                                            gifUrl: {
                                                S: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                                            },
                                            id: {
                                                S: '0007',
                                            },
                                            bodyPart: {
                                                S: 'back',
                                            },
                                            target: {
                                                S: 'lats',
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    });
                });
                describe('When retrieving workouts', () => {
                    let response: Workout[];
                    let expectedResponse: Workout[];

                    beforeEach(async () => {
                        expectedResponse = [
                            {
                                name: 'workout1',
                                exercises: [
                                    {
                                        name: 'alternate lateral pulldown',
                                        equipment: 'cable',
                                        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                                        id: '0007',
                                        bodyPart: 'back',
                                        target: 'lats',
                                    },
                                ],
                            },
                        ];
                        response = await dynamoService.getWorkouts(email);
                    });

                    it('should return the workouts', () => {
                        expect(response).toEqual(expectedResponse);
                    });
                });
            });

            describe('And the email does NOT exist in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({});
                });
                describe('When retrieving workouts', () => {
                    let response: Workout[];
                    beforeEach(async () => {
                        response = await dynamoService.getWorkouts(email);
                    });

                    it('should return no workouts', () => {
                        expect(response).toEqual([]);
                    });
                });
            });
        });
    });

    describe('Unit: getPopularExercises', () => {
        describe('Given an email', () => {
            let email: string;

            beforeEach(() => {
                email = 'email';
            });

            describe('And the email exists in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({
                        Item: {
                            password: {
                                S: 'password',
                            },
                            email: {
                                S: 'test@test.com',
                            },
                            workout1: {
                                L: [
                                    {
                                        M: {
                                            name: {
                                                S: 'alternate lateral pulldown',
                                            },
                                            equipment: {
                                                S: 'cable',
                                            },
                                            gifUrl: {
                                                S: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                                            },
                                            id: {
                                                S: '0007',
                                            },
                                            bodyPart: {
                                                S: 'back',
                                            },
                                            target: {
                                                S: 'lats',
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    });
                });
                describe('When retrieving popular exercises', () => {
                    let response: Exercise[];
                    let expectedResponse: Exercise[];

                    beforeEach(async () => {
                        expectedResponse = [
                            {
                                name: 'alternate lateral pulldown',
                                equipment: 'cable',
                                gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                                id: '0007',
                                bodyPart: 'back',
                                target: 'lats',
                            },
                        ];

                        response = await dynamoService.getPopularExercises(
                            email
                        );
                    });

                    it('should return the exercises', () => {
                        expect(response).toEqual(expectedResponse);
                    });
                });
            });

            describe('And the email does NOT exist in Dynamo', () => {
                beforeEach(() => {
                    dynamoMock.onAnyCommand().resolves({});
                });
                describe('When retrieving popular exercises', () => {
                    let response: Exercise[];
                    beforeEach(async () => {
                        response = await dynamoService.getPopularExercises(
                            email
                        );
                    });

                    it('should return no exercises', () => {
                        expect(response).toEqual([]);
                    });
                });
            });
        });
    });

    describe('Unit: formatExercises', () => {
        describe('Given exercises to format', () => {
            let exercise: Exercise;

            beforeEach(() => {
                exercise = {
                    name: 'alternate lateral pulldown',
                    equipment: 'cable',
                    gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                    id: '0007',
                    bodyPart: 'back',
                    target: 'lats',
                };
            });

            describe('When formatting the exercises', () => {
                let response;
                let expectedResponse;

                beforeEach(() => {
                    expectedResponse = {
                        M: {
                            name: {
                                S: 'alternate lateral pulldown',
                            },
                            equipment: {
                                S: 'cable',
                            },
                            gifUrl: {
                                S: 'http://d205bpvrqc9yn1.cloudfront.net/0007.gif',
                            },
                            id: {
                                S: '0007',
                            },
                            bodyPart: {
                                S: 'back',
                            },
                            target: {
                                S: 'lats',
                            },
                        },
                    };
                    response = dynamoService.formatExercises([exercise]);
                });

                it('should format the exercises', () => {
                    expect(response).toEqual([expectedResponse]);
                });
            });
        });
    });
});
