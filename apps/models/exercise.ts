export interface ExerciseResponse {
    exercises: Exercise[];
}

export interface Exercise {
    bodyPart: string;
    equipment: string;
    gifUrl: string;
    id: string;
    name: string;
    target: string;
}

export interface PopularExerciseResponse {
    exercises: PopularExercise[];
}

export interface PopularExercise {
    exerciseName: string;
    exerciseUsedCount: number;
}
