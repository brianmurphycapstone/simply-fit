import { AccountCreationStatus } from './enums';
export interface Account {
    email: string;
    password: string;
}

export interface AccountCreationResponse {
    status: AccountCreationStatus;
}
