import { SaveWorkoutStatus } from './enums';
import { Exercise } from './exercise';

export interface Workout {
    name: string;
    exercises: Exercise[];
}

export interface SaveWorkoutResponse {
    status: SaveWorkoutStatus;
}

export interface GetWorkoutResponse {
    workouts: Workout[];
}
