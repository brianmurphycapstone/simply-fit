import { LoginResponse } from './../../../models/login';

describe('landing-page', () => {
    before(() => cy.visit('/'));

    describe('E2E: Buttons', () => {
        it('should display a create account button', () => {
            cy.get('[data-cy=create-account-button]').contains(
                'Create Account'
            );
        });

        it('should display a log in button', () => {
            cy.get('[data-cy=log-in-button]').contains('Log in');
        });

        it('should display a find exercises button', () => {
            cy.get('[data-cy=find-exercises-button]').contains(
                'Find Exercises'
            );
        });

        describe('When the user selects the "Create Account" button', () => {
            beforeEach(() => {
                cy.get('[data-cy=create-account-button]').click();
            });

            it('should navigate to the registration page', () => {
                cy.location('href').should('contain', '/register');
                cy.go('back');
            });
        });

        describe('When the user selects the "Log in" button', () => {
            beforeEach(() => {
                cy.get('[data-cy=log-in-button]').click();
            });

            it('should navigate to the login page', () => {
                cy.location('href').should('contain', '/login');
                cy.go('back');
            });
        });

        describe('When the user selects the "Find Exercises" button', () => {
            beforeEach(() => {
                cy.get('[data-cy=find-exercises-button]').click();
            });

            it('should navigate to the login page', () => {
                cy.location('href').should('contain', '/findExercises');
                cy.go('back');
            });
        });

        describe('Given the user is not logged in', () => {
            it('should show a disabled create workout button', () => {
                cy.get('[data-cy=create-workout-button]').should('be.disabled');
            });

            it('should show a disabled view workouts button', () => {
                cy.get('[data-cy=view-workout-button]').should('be.disabled');
            });
        });

        describe('Given the user is logged in', () => {
            before(() => {
                const mockLoginResponse: LoginResponse = {
                    credentialsValid: true,
                };
                cy.intercept('POST', '/api/login', mockLoginResponse);
                cy.on('window:alert', cy.stub());
                cy.visit('/login');
                cy.get('[data-cy=login-email-input]')
                    .clear()
                    .type('test@test.com');
                cy.get('[data-cy=login-password-input]')
                    .clear()
                    .type('password');
                cy.get('[data-cy=login-button]').click();
            });

            it('should show an enabled create workout button', () => {
                cy.get('[data-cy=create-workout-button]').should(
                    'not.be.disabled'
                );
            });

            it('should show a disabled view workouts button', () => {
                cy.get('[data-cy=view-workout-button]').should(
                    'not.be.disabled'
                );
            });

            describe('When the user selects the Create Workout button', () => {
                beforeEach(() => {
                    cy.get('[data-cy=create-workout-button]').click();
                });

                it('should navigate to the Create Workout page', () => {
                    cy.location('href').should('contain', '/createWorkout');
                    cy.go('back');
                });
            });

            describe('When the user selects the View Workouts button', () => {
                beforeEach(() => {
                    cy.get('[data-cy=view-workout-button]').click();
                });

                it('should navigate to the Create Workout page', () => {
                    cy.location('href').should('contain', '/viewWorkouts');
                    cy.go('back');
                });
            });
        });
    });
});
