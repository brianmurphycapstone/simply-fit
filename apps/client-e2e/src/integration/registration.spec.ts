import { AccountCreationStatus } from '../../../models/enums';
import { AccountCreationResponse } from '../../../models/account';
describe('Registration Page', () => {
    before(() => {
        cy.visit('/register');
    });

    it('should display a text input field for email with a label and help text', () => {
        cy.get('[data-cy=registration-email-label]').contains('Email address');
        cy.get('[data-cy=registration-email-input]').should('exist');
        cy.get('[data-cy=registration-email-help]').contains(
            'Enter the email address of the new account.'
        );
    });

    it('should display a text input field for password with a label and help text', () => {
        cy.get('[data-cy=registration-password-label]').contains('Password');
        cy.get('[data-cy=registration-password-input]').should('exist');
        cy.get('[data-cy=registration-password-help]').contains(
            'Your password must be 8-20 alpha-numeric characters long.'
        );
    });

    it('should display a create account', () => {
        cy.get('[data-cy=registration-create-button]').contains(
            'Create Account'
        );
    });

    describe('Given no email is provided', () => {
        describe('And no password is provided', () => {
            beforeEach(() => {
                cy.get('[data-cy=registration-email-input]').click();
            });
            describe('And no password is provided', () => {
                beforeEach(() => {
                    cy.get('[data-cy=registration-password-input]').click();
                    cy.focused().click();
                });
                it('should disable the create account button', () => {
                    cy.get('[data-cy=registration-create-button]').should(
                        'be.disabled'
                    );
                });

                it('should display a warning that email is required', () => {
                    cy.get(
                        '[data-cy=registration-email-required-warning'
                    ).contains('Email is required');
                });

                it('should display a warning that password is required', () => {
                    cy.get(
                        '[data-cy=registration-password-required-warning'
                    ).contains('Password is required');
                });
            });
        });
    });

    describe('Given an invalid email is provided', () => {
        before(() => {
            cy.get('[data-cy=registration-email-input]').type('invalid email');
            cy.focused().click();
        });

        it('should display a warning to enter a valid email', () => {
            cy.get('[data-cy=registration-email-valid-warning').contains(
                'Enter a valid email'
            );
        });

        it('should disable the create account button', () => {
            cy.get('[data-cy=registration-create-button]').should(
                'be.disabled'
            );
        });
    });

    describe('Given a valid email is provided', () => {
        before(() => {
            cy.get('[data-cy=registration-email-input]')
                .clear()
                .type('test@test.com');
            cy.focused().click();
        });

        describe('And no password is provided', () => {
            it('should disable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.disabled'
                );
            });
        });

        describe('And a password is provided with less than 8 characters', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('asdf');
            });

            it('should display a warning that password must have a minimum of 8 characters', () => {
                cy.get(
                    '[data-cy=registration-password-min-length-warning'
                ).contains('Password should have minimum 8 characters');
            });

            it('should disable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.disabled'
                );
            });
        });

        describe('And a password is provided with more than 20 characters', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('thispasswordistoolong');
            });

            it('should display a warning that password must have a maximum of 20 characters', () => {
                cy.get(
                    '[data-cy=registration-password-max-length-warning'
                ).contains('Password should have maximum 20 characters');
            });

            it('should disable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.disabled'
                );
            });
        });

        describe('And a password is provided with more special symbols', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('!!!!!');
            });

            it('should display a warning that password should only contain alpha numeric characters', () => {
                cy.get(
                    '[data-cy=registration-password-alpha-numeric-warning'
                ).contains('Password should only catain letters and numbers');
            });

            it('should disable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.disabled'
                );
            });
        });

        describe('And a valid password is provided', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('password');
            });

            it('should enable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.enabled'
                );
            });
        });
    });

    describe('Given an email that was already used for an account', () => {
        before(() => {
            cy.get('[data-cy=registration-email-input]')
                .clear()
                .type('existingEmail@test.com');
            cy.focused().click();
        });

        describe('And a valid password is provided', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('password');
            });

            it('should enable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.enabled'
                );
            });

            describe('When the user tries to create an account', () => {
                let alertStub: any;
                before(() => {
                    const mockCreateAccountResponse: AccountCreationResponse = {
                        status: AccountCreationStatus.ACCOUNT_ALREADY_EXISTS,
                    };
                    cy.intercept(
                        'POST',
                        '/api/createAccount',
                        mockCreateAccountResponse
                    );
                    alertStub = cy.stub();
                    cy.on('window:alert', alertStub);
                    cy.get('[data-cy=registration-create-button]').click();
                });

                it('should alert an account already exists for the email', () => {
                    expect(alertStub.getCall(0)).to.be.calledWith(
                        'Account already exists, please try a new email or log in with existing credentials.'
                    );
                });

                it('should reset the email and password fields', () => {
                    cy.get('[data-cy=registration-email-input]').should(
                        'be.empty'
                    );
                    cy.get('[data-cy=registration-password-input]').should(
                        'be.empty'
                    );
                });
            });
        });
    });

    describe('Given an email is not already used for an account', () => {
        before(() => {
            cy.get('[data-cy=registration-email-input]')
                .clear()
                .type('newEmail@test.com');
            cy.focused().click();
        });

        describe('And a valid password is provided', () => {
            before(() => {
                cy.get('[data-cy=registration-password-input]')
                    .clear()
                    .type('password');
            });

            it('should enable the create account button', () => {
                cy.get('[data-cy=registration-create-button]').should(
                    'be.enabled'
                );
            });

            describe('When the user tries to create an account', () => {
                let alertStub: any;
                before(() => {
                    const mockCreateAccountResponse: AccountCreationResponse = {
                        status: AccountCreationStatus.ACCOUNT_CREATED,
                    };
                    cy.intercept(
                        'POST',
                        '/api/createAccount',
                        mockCreateAccountResponse
                    );
                    alertStub = cy.stub();
                    cy.on('window:alert', alertStub);
                    cy.get('[data-cy=registration-create-button]').click();
                });

                it('should alert that account creation was successful', () => {
                    expect(alertStub.getCall(0)).to.be.calledWith(
                        'Account created successfully!'
                    );
                });

                it('should navigate to the login page', () => {
                    cy.location('href').should('contain', '/login');
                });
            });
        });
    });
});
