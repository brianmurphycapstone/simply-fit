import { Exercise } from '../../../models/exercise';

describe('create-workout', () => {
    const mockExercise: Exercise = {
        name: 'push up on bosu ball',
        equipment: 'bosu ball',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1307.gif',
        id: '1307',
        bodyPart: 'chest',
        target: 'pectorals',
    };

    before('Given a user is on the create workout page', () => {
        cy.intercept('GET', '/api/exercises', { exercises: [mockExercise] });
        cy.intercept('POST', '/api/exercises/popular', []);
        cy.visit('/createWorkout');
    });

    it('should display an input to enter a workout name', () => {
        cy.get('[data-cy=create-workout-name]').should('exist');
    });

    it('should display an empty list that indicates the user needs to add exercises', () => {
        cy.get('[data-cy=create-workout-list]').contains(
            'Please add exercises'
        );
    });

    it('should display a button to manually add exercises', () => {
        cy.get('[data-cy=create-workout-manual-button]').should('be.enabled');
    });

    it('should display a button to generate a full body workout', () => {
        cy.get('[data-cy=create-workout-generate-button]').should('be.enabled');
    });

    it('should display a DISABLED button to reset the workout', () => {
        cy.get('[data-cy=create-workout-reset-button]').should('be.disabled');
    });

    it('should display a DISABLED button to save the workout', () => {
        cy.get('[data-cy=create-workout-save-button]').should('be.disabled');
    });

    describe('When a user clicks the "Mannually Add Exercises" button', () => {
        before(() => {
            cy.get('[data-cy=create-workout-manual-button]').click();
        });

        it('should display a modal with a list of exercises', () => {
            cy.get('[data-cy=create-workout-modal]').should(
                'have.class',
                'show'
            );
        });

        it('should display a modal header with an input to filter exercises', () => {
            cy.get('[data-cy=create-workout-modal-header-input]').should(
                'exist'
            );
        });

        it('should display a modal header with a dropdown to select body part', () => {
            cy.get('[data-cy=create-workout-modal-header-bodypart]').should(
                'exist'
            );
        });

        it('should display a modal header with a dropdown to select equipment', () => {
            cy.get('[data-cy=create-workout-modal-header-equipment]').should(
                'exist'
            );
        });

        it('should display a modal header with a dropdown to select target muscle', () => {
            cy.get('[data-cy=create-workout-modal-header-target]').should(
                'exist'
            );
        });

        it('should display a modal header with a button to reset filters', () => {
            cy.get('[data-cy=create-workout-modal-header-reset]').should(
                'exist'
            );
        });

        it('should display a modal footer with a button to generate a full body workout', () => {
            cy.get('[data-cy=create-workout-modal-footer-generate]').should(
                'exist'
            );
        });

        it('should display a modal footer with a button to save changes', () => {
            cy.get('[data-cy=create-workout-modal-footer-save]').should(
                'exist'
            );
        });

        describe('When exercises are selected and saved', () => {
            before(() => {
                cy.get('[data-cy=create-workout-modal-body-exercise]').click();
                // eslint-disable-next-line cypress/no-unnecessary-waiting
                cy.wait(150);
                cy.get('[data-cy=create-workout-modal-footer-save]').click({
                    force: true,
                });
            });

            it('should close the create workout modal', () => {
                cy.get('[data-cy=create-workout-modal]').should(
                    'not.have.class',
                    'show'
                );
            });

            it('should add the selected exercises to the workout', () => {
                cy.get('[data-cy=create-workout-list]').contains(
                    'push up on bosu ball'
                );
            });

            it('should ENABLE the button to reset the workout', () => {
                cy.get('[data-cy=create-workout-reset-button]').should(
                    'be.enabled'
                );
            });

            it('should ENABLE the button to save the workout', () => {
                cy.get('[data-cy=create-workout-save-button]').should(
                    'be.enabled'
                );
            });
        });
    });
});
