import { Exercise } from '../../../models/exercise';

/* eslint-disable cypress/no-unnecessary-waiting */
describe('exercise-display', () => {
    const mockExercise: Exercise = {
        name: 'push up on bosu ball',
        equipment: 'bosu ball',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1307.gif',
        id: '1307',
        bodyPart: 'chest',
        target: 'pectorals',
    };
    describe('Given a user has searched for exercises', () => {
        before(() => {
            cy.intercept('GET', '/api/exercises', {
                exercises: [mockExercise],
            });
            cy.intercept('GET', '/api/exercises/equipment/bosu%20ball', {
                exercises: [mockExercise],
            });
            cy.intercept('POST', '/api/exercises/popular', []);
            cy.visit('/findExercises');
            cy.get('[data-cy=find-equipment-select]').select('bosu ball');
            cy.wait(150);
            cy.get('[data-cy=find-equipment-search-button]').click({
                force: true,
            });
        });

        it('should display a list of exercises as accordion buttons', () => {
            cy.get('[data-cy=display-exercise-button]').should('exist');
        });

        describe('When a user clicks on an exercise', () => {
            before(() => {
                cy.wait(150);
                cy.get('[data-cy=display-exercise-button]').first().click({
                    force: true,
                });
            });

            it('should expand the exercise accordion', () => {
                cy.get('[data-cy=display-exercise-button]')
                    .invoke('attr', 'aria-expanded')
                    .should('eq', 'true');
            });

            it('should display the exercise body part in the body', () => {
                cy.get('[data-cy=display-exercise-accordion]').contains(
                    'chest'
                );
            });

            it('should display the exercise equipment in the body', () => {
                cy.get('[data-cy=display-exercise-accordion]').contains(
                    'bosu ball'
                );
            });

            it('should display the exercise target in the body', () => {
                cy.get('[data-cy=display-exercise-accordion]').contains(
                    'pectorals'
                );
            });

            it('should display an animation of the exercise movement in the body', () => {
                cy.get('[data-cy=display-exercise-accordion]').should('exist');
            });
        });
    });
});
