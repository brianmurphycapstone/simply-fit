import { Exercise } from './../../../models/exercise';
import { GetWorkoutResponse, Workout } from './../../../models/workout';
import { LoginResponse } from '../../../models/login';

describe('view-workouts', () => {
    before(() => {
        cy.intercept('GET', '/api/exercises', []);
        cy.visit('/');
        const mockLoginResponse: LoginResponse = {
            credentialsValid: true,
        };
        cy.intercept('POST', '/api/login', mockLoginResponse);
        cy.on('window:alert', cy.stub());
        cy.visit('/login');
        cy.get('[data-cy=login-email-input]').clear().type('test@test.com');
        cy.get('[data-cy=login-password-input]').clear().type('password');
        cy.get('[data-cy=login-button]').click();
    });

    describe('Given a user has NO workouts', () => {
        before(() => {
            const mockWorkoutsResposne: GetWorkoutResponse = {
                workouts: [],
            };
            cy.intercept('POST', '/api/getWorkouts/', mockWorkoutsResposne);
            cy.visit('/viewWorkouts');
        });

        it('should display a button that says "No Saved Workouts! Create a new Workout."', () => {
            cy.get('[data-cy=view-no-saved-workouts]').contains(
                'No Saved Workouts! Create a new Workout.'
            );
        });

        describe('When the "No Saved Workouts! Create a new Workout." button it clicked', () => {
            before(() => {
                cy.get('[data-cy=view-no-saved-workouts]').click();
            });

            it('should navigate to the create workout page', () => {
                cy.location('href').should('contain', '/createWorkout');
            });
        });
    });

    describe('Given a user has workouts', () => {
        const mockExercise: Exercise = {
            name: 'push up on bosu ball',
            equipment: 'bosu ball',
            gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1307.gif',
            id: '1307',
            bodyPart: 'chest',
            target: 'pectorals',
        };

        before(() => {
            const mockWorkout: Workout = {
                name: 'mockWorkout',
                exercises: [mockExercise],
            };

            const mockWorkoutsResposne: GetWorkoutResponse = {
                workouts: [mockWorkout],
            };
            cy.intercept('POST', '/api/getWorkouts/', mockWorkoutsResposne);
            cy.visit('/viewWorkouts');
        });

        it('should display button(s) with the workout name(s)', () => {
            cy.get('[data-cy=view-saved-workout]').contains('mockWorkout');
        });

        describe('When a workout button is clicked', () => {
            before(() => {
                cy.get('[data-cy=view-saved-workout]').click();
            });

            it('should display a modal for the workout', () => {
                cy.get('[data-cy=view-workout-modal]').should('exist');
            });

            it('should contain the workout name in the header', () => {
                cy.get('[data-cy=view-workout-modal-header]').contains(
                    'mockWorkout'
                );
            });

            it('should list the workout exercises', () => {
                cy.get('[data-cy=view-workout-modal-exercise]').contains(
                    mockExercise.name
                );
            });

            it('should display a link to view exercise details on each exercise', () => {
                cy.get(
                    '[data-cy=view-workout-modal-exercise-details]'
                ).contains('Exercise Details');
            });

            describe('When the user clicks the link to view exercise details', () => {
                before(() => {
                    cy.get(
                        '[data-cy=view-workout-modal-exercise-details]'
                    ).click();
                });

                it('should display a modal for exercise details', () => {
                    cy.get('[data-cy=view-exercise-details-modal]').should(
                        'exist'
                    );
                });

                it('should display the exercise name in the header', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-header]'
                    ).contains(mockExercise.name);
                });

                it('should display the exercise body part in the body', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-body]'
                    ).contains(mockExercise.bodyPart);
                });

                it('should display the exercise equipment in the body', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-body]'
                    ).contains(mockExercise.equipment);
                });

                it('should display the exercise target in the body', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-body]'
                    ).contains(mockExercise.target);
                });

                it('should display an animation of the exercise movement in the body', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-animation]'
                    ).should('exist');
                });

                it('should contain a back button in the exercise modal footer', () => {
                    cy.get(
                        '[data-cy=view-exercise-details-modal-footer]'
                    ).contains('Back');
                });

                describe('When the back button is clicked', () => {
                    before(() => {
                        cy.get(
                            '[data-cy=view-exercise-details-modal-back]'
                        ).click();
                    });

                    it('should return to the workout modal', () => {
                        cy.get('[data-cy=view-workout-modal]').should('exist');
                    });
                });
            });
        });
    });
});
