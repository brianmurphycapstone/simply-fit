import { LoginResponse } from '../../../models/login';

describe('navbar', () => {
    before(() => cy.visit('/'));

    describe('Given a user is NOT logged in', () => {
        it('should dispaly the navigation bar with links for home, find exercises and log in', () => {
            cy.get('[data-cy=navbar]');
            cy.get('[data-cy=navbar-home]').contains('Home');
            cy.get('[data-cy=navbar-find-exercises]').contains(
                'Find Exercises'
            );
            cy.get('[data-cy=navbar-log-in]').contains('Log in');
        });

        it('should NOT display the navigation bar with links for create workout, view workouts and log out', () => {
            cy.get('[data-cy=navbar-create-workout]').should('not.exist');
            cy.get('[data-cy=navbar-view-workouts]').should('not.exist');
            cy.get('[data-cy=navbar-log-out]').should('not.exist');
        });

        describe('When the user selects "Home" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-home').click();
            });

            it('should navigate to the landing page', () => {
                cy.location('href').should('eq', 'http://localhost:4200/');
            });
        });

        describe('When the user selects "Find Exercises" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-find-exercises').click();
            });

            it('should navigate to the find exercises page', () => {
                cy.location('href').should('contain', '/findExercises');
            });
        });

        describe('When the user selects "Log in" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-log-in').click();
            });

            it('should navigate to the login page', () => {
                cy.location('href').should('contain', '/login');
            });
        });
    });

    describe('Given a user is logged in', () => {
        before(() => {
            const mockLoginResponse: LoginResponse = {
                credentialsValid: true,
            };
            cy.intercept('POST', '/api/login', mockLoginResponse);
            cy.on('window:alert', cy.stub());
            cy.visit('/login');
            cy.get('[data-cy=login-email-input]').clear().type('test@test.com');
            cy.get('[data-cy=login-password-input]').clear().type('password');
            cy.get('[data-cy=login-button]').click();
        });

        it('should dispaly the navigation bar with links for home, find exercises, create workout, view workouts and log in', () => {
            cy.get('[data-cy=navbar]');
            cy.get('[data-cy=navbar-home]').contains('Home');
            cy.get('[data-cy=navbar-find-exercises]').contains(
                'Find Exercises'
            );
            cy.get('[data-cy=navbar-create-workout]').contains(
                'Create Workout'
            );
            cy.get('[data-cy=navbar-view-workouts]').contains('View Workouts');
            cy.get('[data-cy=navbar-log-out]').contains('Log Out');
        });

        it('should NOT display the navigation bar with a link to log in', () => {
            cy.get('[data-cy=navbar-log-in]').should('not.exist');
        });

        it('should display the logged in email address', () => {
            cy.get('[data-cy=navbar-logged-in-email]').contains(
                'test@test.com'
            );
        });

        describe('When the user selects "Home" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-home').click();
            });

            it('should navigate to the landing page', () => {
                cy.location('href').should('eq', 'http://localhost:4200/');
            });
        });

        describe('When the user selects "Find Exercises" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-find-exercises').click();
            });

            it('should navigate to the find exercises page', () => {
                cy.location('href').should('contain', '/findExercises');
            });
        });

        describe('When the user selects "Create Workout" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-create-workout').click();
            });

            it('should navigate to the create workout page', () => {
                cy.location('href').should('contain', '/createWorkout');
            });
        });

        describe('When the user selects "View Workouts" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-view-workouts').click();
            });

            it('should navigate to the view workouts page', () => {
                cy.location('href').should('contain', '/viewWorkouts');
            });
        });

        describe('When the user selects "Log Out" from the navbar', () => {
            beforeEach(() => {
                cy.get('[data-cy=navbar-log-out').click();
            });

            it('should navigate to the home page', () => {
                cy.location('href').should('eq', 'http://localhost:4200/');
            });
        });
    });
});
