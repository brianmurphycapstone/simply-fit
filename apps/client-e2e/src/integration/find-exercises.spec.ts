import { Exercise } from '../../../models/exercise';

/* eslint-disable cypress/no-unnecessary-waiting */
describe('find-exercises', () => {
    const mockExercise: Exercise = {
        name: 'push up on bosu ball',
        equipment: 'bosu ball',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1307.gif',
        id: '1307',
        bodyPart: 'chest',
        target: 'pectorals',
    };
    before(() => {
        cy.intercept('GET', '/api/exercises', { exercises: [mockExercise] });
        cy.intercept('GET', '/api/exercises/equipment/bosu%20ball', {
            exercises: [mockExercise],
        });
        cy.intercept('POST', '/api/exercises/popular', []);
        cy.visit('/findExercises');
    });

    it('should display an accordion button to find exercises by equipment', () => {
        cy.get('[data-cy=find-equipment-button]').should('exist');
    });

    it('should display an accordion button to find exercises by body part', () => {
        cy.get('[data-cy=find-body-part-button]').should('exist');
    });

    it('should display an accordion button to find exercises by target muscle group', () => {
        cy.get('[data-cy=find-target-button]').should('exist');
    });

    describe('When the find exercises by target muscle button is clicked', () => {
        before(() => {
            cy.wait(150);
            cy.get('[data-cy=find-target-button]').click({
                force: true,
            });
        });

        it('should expand the find exercises by target accordion', () => {
            cy.get('[data-cy=find-target-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'true');
        });

        it('should collapse the other accordion buttons', () => {
            cy.get('[data-cy=find-body-part-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');

            cy.get('[data-cy=find-equipment-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');
        });
    });

    describe('When the find exercises by body part button is clicked', () => {
        before(() => {
            cy.wait(150);
            cy.get('[data-cy=find-body-part-button]').click({
                force: true,
            });
        });

        it('should expand the find exercises by body part accordion', () => {
            cy.get('[data-cy=find-body-part-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'true');
        });

        it('should collapse the other accordion buttons', () => {
            cy.get('[data-cy=find-target-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');

            cy.get('[data-cy=find-equipment-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');
        });
    });

    describe('When the find exercises by equipment button is clicked', () => {
        before(() => {
            cy.wait(150);
            cy.get('[data-cy=find-equipment-button]').click({
                force: true,
            });
        });

        it('should expand the find exercises by equipment accordion', () => {
            cy.get('[data-cy=find-equipment-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'true');
        });

        it('should collapse the other accordion buttons', () => {
            cy.get('[data-cy=find-target-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');

            cy.get('[data-cy=find-body-part-button]')
                .invoke('attr', 'aria-expanded')
                .should('eq', 'false');
        });
    });

    describe('Given the user searches for exercises', () => {
        before(() => {
            cy.get('[data-cy=find-equipment-select]').select('bosu ball');
            cy.wait(150);
            cy.get('[data-cy=find-equipment-search-button]').click({
                force: true,
            });
        });

        it('should navigate to the exercise display page', () => {
            cy.location('href').should('contain', '/');
        });
    });
});
