import { LoginResponse } from './../../../models/login';
describe('Log In Page', () => {
    before(() => {
        cy.visit('/login');
    });

    it('should display a text input field for email with a label and help text', () => {
        cy.get('[data-cy=login-email-label]').contains('Email address');
        cy.get('[data-cy=login-email-input]').should('exist');
        cy.get('[data-cy=login-email-help]').contains(
            'Enter the email address of the account.'
        );
    });

    it('should display a text input field for password with a label', () => {
        cy.get('[data-cy=login-password-label]').contains('Password');
        cy.get('[data-cy=login-password-input]').should('exist');
    });

    it('should display a log in button', () => {
        cy.get('[data-cy=login-button]').contains('Log in');
    });

    describe('Given no email is provided', () => {
        beforeEach(() => {
            cy.get('[data-cy=login-email-input]').click();
        });
        describe('And no password is provided', () => {
            beforeEach(() => {
                cy.get('[data-cy=login-password-input]').click();
                cy.focused().click();
            });
            it('should disable to log in button', () => {
                cy.get('[data-cy=login-button]').should('be.disabled');
            });

            it('should display a warning that email is required', () => {
                cy.get('[data-cy=login-email-required-warning').contains(
                    'Email is required'
                );
            });

            it('should display a warning that password is required', () => {
                cy.get('[data-cy=login-password-required-warning').contains(
                    'Password is required'
                );
            });
        });
    });

    describe('Given an invalid email is provided', () => {
        before(() => {
            cy.get('[data-cy=login-email-input]').type('invalid email');
            cy.focused().click();
        });

        it('should display a warning to enter a valid email', () => {
            cy.get('[data-cy=login-email-valid-warning').contains(
                'Enter a valid email'
            );
        });

        it('should disable the log in button', () => {
            cy.get('[data-cy=login-button]').should('be.disabled');
        });
    });

    describe('Given a valid email is provided', () => {
        before(() => {
            cy.get('[data-cy=login-email-input]').clear().type('test@test.com');
            cy.focused().click();
        });

        describe('And no password is provided', () => {
            it('should disable the log in button', () => {
                cy.get('[data-cy=login-button]').should('be.disabled');
            });
        });

        describe('And a password is provided', () => {
            before(() => {
                cy.get('[data-cy=login-password-input]').type('password');
            });

            it('should enable the log in button', () => {
                cy.get('[data-cy=login-button]').should('be.enabled');
            });
        });
    });

    describe('Given an email or password that is not a valid account', () => {
        before(() => {
            cy.get('[data-cy=login-email-input]')
                .clear()
                .type('noaccount@test.com');
            cy.get('[data-cy=login-password-input]').clear().type('password');
        });

        describe('When logging in', () => {
            let alertStub: any;
            before(() => {
                const mockLoginResponse: LoginResponse = {
                    credentialsValid: false,
                };
                cy.intercept('POST', '/api/login', mockLoginResponse);
                alertStub = cy.stub();
                cy.on('window:alert', alertStub);
                cy.get('[data-cy=login-button]').click();
            });

            it('should alert log in failed', () => {
                expect(alertStub.getCall(0)).to.be.calledWith(
                    'Log in failed. Please try again.'
                );
            });

            it('should reset the email and password fields', () => {
                cy.get('[data-cy=login-email-input]').should('be.empty');
                cy.get('[data-cy=login-password-input]').should('be.empty');
            });
        });
    });

    describe('Given a valid account', () => {
        before(() => {
            cy.get('[data-cy=login-email-input]').clear().type('test@test.com');
            cy.get('[data-cy=login-password-input]').clear().type('password');
        });

        describe('When logging in', () => {
            let alertStub: any;
            before(() => {
                const mockLoginResponse: LoginResponse = {
                    credentialsValid: true,
                };
                cy.intercept('POST', '/api/login', mockLoginResponse);
                alertStub = cy.stub();
                cy.on('window:alert', alertStub);
                cy.get('[data-cy=login-button]').click();
            });

            it('should alert log in succeeded', () => {
                expect(alertStub.getCall(0)).to.be.calledWith(
                    'Logged in successfully!'
                );
            });

            it('should navigate to the landing page', () => {
                cy.location('href').should('eq', 'http://localhost:4200/');
            });
        });
    });
});
