import { Exercise } from './../../../../../models/exercise';
import { SaveWorkoutStatus } from './../../../../../models/enums';
import { FilterExercisesPipe } from './../../pipes/filter-exercises.pipe';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExercisesService } from '../../services/exercises.service';
import { CreateWorkoutComponent } from './create-workout.component';
import { of, throwError } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { RequestService } from '../../services/request.service';
import { Router } from '@angular/router';

const mockFullBodyWorkout = [
    {
        bodyPart: 'back',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/3297.gif',
        id: '3297',
        name: 'back lever',
        target: 'upper back',
    },
    {
        bodyPart: 'chest',
        equipment: 'kettlebell',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1298.gif',
        id: '1298',
        name: 'kettlebell one arm floor press',
        target: 'pectorals',
    },
    {
        bodyPart: 'lower arms',
        equipment: 'dumbbell',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0364.gif',
        id: '0364',
        name: 'dumbbell one arm wrist curl',
        target: 'forearms',
    },
    {
        bodyPart: 'upper arms',
        equipment: 'dumbbell',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1668.gif',
        id: '1668',
        name: 'dumbbell one arm seated bicep curl on exercise ball',
        target: 'biceps',
    },
    {
        bodyPart: 'lower legs',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1368.gif',
        id: '1368',
        name: 'ankle circles',
        target: 'calves',
    },
    {
        bodyPart: 'upper legs',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/1422.gif',
        id: '1422',
        name: 'pelvic tilt into bridge',
        target: 'glutes',
    },
    {
        bodyPart: 'shoulders',
        equipment: 'kettlebell',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0550.gif',
        id: '0550',
        name: 'kettlebell thruster',
        target: 'delts',
    },
    {
        bodyPart: 'neck',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0716.gif',
        id: '0716',
        name: 'side push neck stretch',
        target: 'levator scapulae',
    },
    {
        bodyPart: 'waist',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0002.gif',
        id: '0002',
        name: '45 side bend',
        target: 'abs',
    },
    {
        bodyPart: 'cardio',
        equipment: 'body weight',
        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0002.gif',
        id: '0002',
        name: '45 side bend',
        target: 'abs',
    },
];

const mockExercisesService = {
    getAllExercises: jest.fn().mockReturnValue(of(mockFullBodyWorkout)),
};

const mockRequestService = {
    saveWorkout: jest.fn(),
    getPopularExercises: jest.fn().mockReturnValue(
        of({
            exercises: [
                {
                    bodyPart: 'neck',
                    equipment: 'body weight',
                    gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0716.gif',
                    id: '0716',
                    name: 'side push neck stretch',
                    target: 'levator scapulae',
                },
            ],
        })
    ),
};

const mockLoginService = {
    getAccountEmail: jest
        .fn()
        .mockReturnValue(of({ email: 'test@test.com', password: '' })),
};

const mockRouter = {
    navigate: jest.fn(),
};

describe('CreateWorkoutComponent', () => {
    let createWorkoutComponent: CreateWorkoutComponent;
    let fixture: ComponentFixture<CreateWorkoutComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [FormsModule],
            providers: [
                { provide: ExercisesService, useValue: mockExercisesService },
                { provide: RequestService, useValue: mockRequestService },
                { provide: LoginService, useValue: mockLoginService },
                { provide: Router, useValue: mockRouter },
            ],
            declarations: [CreateWorkoutComponent, FilterExercisesPipe],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateWorkoutComponent);
        createWorkoutComponent = fixture.componentInstance;
    });

    it('should create', () => {
        expect(createWorkoutComponent).toBeTruthy();
    });

    it('should retrieve all exercises', () => {
        fixture.detectChanges();
        expect(mockExercisesService.getAllExercises).toHaveBeenCalled();
    });

    describe('Unit: ngOnInit', () => {
        describe('Name of the group', () => {
            describe('Given an exercise with no bodypart', () => {
                let exercise: Exercise;
                beforeEach(() => {
                    exercise = {
                        equipment: 'body weight',
                        gifUrl: 'http://d205bpvrqc9yn1.cloudfront.net/0002.gif',
                        id: '0002',
                        name: '45 side bend',
                        target: 'abs',
                    } as Exercise;
                });

                describe('When component is initialized', () => {
                    let expectedExercisesByBodyPart: Map<string, Exercise[]>;
                    beforeEach(() => {
                        mockExercisesService.getAllExercises.mockReturnValueOnce(
                            of([exercise])
                        );
                        mockRequestService.getPopularExercises.mockReturnValueOnce(
                            of({ exercises: [exercise] })
                        );
                        expectedExercisesByBodyPart = new Map<
                            string,
                            Exercise[]
                        >([
                            ['back', []],
                            ['chest', []],
                            ['cardio', []],
                            ['lower arms', []],
                            ['upper arms', []],
                            ['lower legs', []],
                            ['upper legs', []],
                            ['shoulders', []],
                            ['neck', []],
                            ['waist', []],
                        ]);
                        createWorkoutComponent.ngOnInit();
                    });

                    it('should not update exercises by bodypart', () => {
                        expect(
                            createWorkoutComponent.exercisesByBodyPart
                        ).toEqual(expectedExercisesByBodyPart);
                    });
                });
            });
        });
    });

    describe('Unit: selectExercise', () => {
        describe('Given an exercise is not aready selected', () => {
            beforeEach(() => {
                createWorkoutComponent.selectedExercises = [];
            });

            describe('When an exercise is selected', () => {
                let selectedExercise: Exercise;
                beforeEach(() => {
                    selectedExercise = {
                        name: 'new exercise',
                    } as unknown as Exercise;
                    createWorkoutComponent.selectExercise(selectedExercise);
                });

                it('should add the selected exercise', () => {
                    expect(createWorkoutComponent.selectedExercises).toEqual([
                        selectedExercise,
                    ]);
                });
            });
        });

        describe('Given an exercise is aready selected', () => {
            let selectedExercise: Exercise;
            beforeEach(() => {
                selectedExercise = {
                    name: 'existing exercise',
                } as unknown as Exercise;
                createWorkoutComponent.selectedExercises = [selectedExercise];
            });

            describe('When an exercise is selected', () => {
                beforeEach(() => {
                    createWorkoutComponent.selectExercise(selectedExercise);
                });

                it('should remove the selected exercise', () => {
                    expect(createWorkoutComponent.selectedExercises).toEqual(
                        []
                    );
                });
            });
        });

        describe('Given an undefined exercise', () => {
            let selectedExercise: Exercise | undefined;
            beforeEach(() => {
                selectedExercise = undefined;
            });

            describe('When an selecting an exercise', () => {
                beforeEach(() => {
                    createWorkoutComponent.selectExercise(selectedExercise);
                });

                it('should NOT update the selected exercises', () => {
                    expect(createWorkoutComponent.selectedExercises).toEqual(
                        []
                    );
                });
            });
        });
    });

    describe('Unit: selectFullBodyWorkoutEquipment', () => {
        describe('Given there are no pieces of equipment selected', () => {
            beforeEach(() => {
                createWorkoutComponent.fullBodyWorkoutSelectedEquipment = [];
            });

            describe('When a piece of equipment is selected', () => {
                let equipment: string;
                beforeEach(() => {
                    equipment = 'barbell';
                    createWorkoutComponent.selectFullBodyWorkoutEquipment(
                        equipment
                    );
                });

                it('should add the selected equipment', () => {
                    expect(
                        createWorkoutComponent.fullBodyWorkoutSelectedEquipment
                    ).toEqual([equipment]);
                });
            });
        });

        describe('Given a piece of equipment is aready selected', () => {
            let equipment: string;
            beforeEach(() => {
                equipment = 'barbell';
                createWorkoutComponent.fullBodyWorkoutSelectedEquipment = [
                    equipment,
                ];
            });

            describe('When a piece of equipment is selected', () => {
                beforeEach(() => {
                    createWorkoutComponent.selectFullBodyWorkoutEquipment(
                        equipment
                    );
                });

                it('should remove the selected piece of equipment', () => {
                    expect(createWorkoutComponent.selectedExercises).toEqual(
                        []
                    );
                });
            });
        });
    });

    describe('Unit: saveChanges', () => {
        describe('Given selected exercises', () => {
            let selectedExercise: Exercise;

            beforeEach(() => {
                selectedExercise = {
                    name: 'selected exercise',
                } as unknown as Exercise;
                createWorkoutComponent.selectedExercises = [selectedExercise];
            });
            describe('When saving changes', () => {
                beforeEach(() => {
                    createWorkoutComponent.saveChanges();
                });

                it('should save the selected exercises', () => {
                    expect(createWorkoutComponent.savedExercises).toEqual([
                        selectedExercise,
                    ]);
                });
            });
        });
    });

    describe('Unit: isChecked', () => {
        describe('Given an exercise has been selected', () => {
            let selectedExercise: Exercise;

            beforeEach(() => {
                selectedExercise = {
                    name: 'selected exercise',
                } as unknown as Exercise;
                createWorkoutComponent.selectedExercises = [selectedExercise];
            });

            describe('When checking if the exercise is checked(selected)', () => {
                let result: boolean;

                beforeEach(() => {
                    result = createWorkoutComponent.isChecked(selectedExercise);
                });

                it('should return that the exercise is checked', () => {
                    expect(result).toEqual(true);
                });
            });
        });

        describe('Given an exercise has NOT been selected', () => {
            let exercise: Exercise;

            beforeEach(() => {
                exercise = {
                    name: 'not selected exercise',
                } as unknown as Exercise;
            });

            describe('When checking if the exercise is checked(selected)', () => {
                let result: boolean;

                beforeEach(() => {
                    result = createWorkoutComponent.isChecked(exercise);
                });

                it('should return that the exercise NOT is checked', () => {
                    expect(result).toEqual(false);
                });
            });
        });

        describe('Given an exercise is not defined', () => {
            let exercise: Exercise | undefined;

            beforeEach(() => {
                exercise = undefined;
            });

            describe('When checking if the undefined exercise is checked(selected)', () => {
                let result: boolean;

                beforeEach(() => {
                    result = createWorkoutComponent.isChecked(exercise);
                });

                it('should return that the exercise is NOT checked', () => {
                    expect(result).toEqual(false);
                });
            });
        });
    });

    describe('Unit: resetFilters', () => {
        describe('Given filters are set', () => {
            beforeEach(() => {
                createWorkoutComponent.selectedEquipment = 'test';
                createWorkoutComponent.selectedBodypart = 'test';
                createWorkoutComponent.selectedTarget = 'test';
            });
            describe('When resetting filters', () => {
                beforeEach(() => {
                    createWorkoutComponent.resetFilters();
                });

                it('should reset the filters for equipment, bodypart and target', () => {
                    expect(createWorkoutComponent.selectedEquipment).toEqual(
                        ''
                    );
                    expect(createWorkoutComponent.selectedBodypart).toEqual('');
                    expect(createWorkoutComponent.selectedTarget).toEqual('');
                });
            });
        });
    });

    describe('Unit: resetWorkout', () => {
        describe('Given exercises are selected and a workout is saved', () => {
            beforeEach(() => {
                createWorkoutComponent.selectedExercises = mockFullBodyWorkout;
                createWorkoutComponent.savedExercises = mockFullBodyWorkout;
            });
            describe('When resetting the workout', () => {
                beforeEach(() => {
                    createWorkoutComponent.resetWorkout();
                });

                it('should reset the selected exercises and saved exercises', () => {
                    expect(createWorkoutComponent.selectedExercises).toEqual(
                        []
                    );
                    expect(createWorkoutComponent.savedExercises).toEqual([]);
                });
            });
        });
    });

    describe('Unit: generateWorkout', () => {
        describe('Given no workout was previously generated', () => {
            beforeEach(() => {
                createWorkoutComponent.generatedWorkout = new Map<
                    string,
                    Exercise
                >();
            });
            describe('When generating a workout', () => {
                beforeEach(() => {
                    createWorkoutComponent.ngOnInit();
                    createWorkoutComponent.generateWorkout();
                });

                it('should generate a full body workout', () => {
                    fixture.detectChanges();
                    const expectedWorkoutMap = new Map<string, Exercise>();

                    mockFullBodyWorkout.forEach((exercise) => {
                        expectedWorkoutMap.set(exercise.bodyPart, exercise);
                    });

                    expect(createWorkoutComponent.generatedWorkout).toEqual(
                        expectedWorkoutMap
                    );
                });
            });
        });
    });

    describe('Unit: getRandomExercise', () => {
        describe('Given undefined exercises', () => {
            const exercises: Exercise[] | undefined = undefined;

            describe('When retrieving a random exercise', () => {
                let response: Exercise | undefined;
                beforeEach(() => {
                    response =
                        createWorkoutComponent.getRandomExercise(exercises);
                });

                it('should return undefined', () => {
                    expect(response).toEqual(undefined);
                });
            });
        });

        describe('Given a list of exercises', () => {
            const exercises: Exercise[] | undefined = mockFullBodyWorkout;

            describe('And no equipment is selected to filter the exercises', () => {
                beforeEach(() => {
                    createWorkoutComponent.fullBodyWorkoutSelectedEquipment =
                        [];
                });

                describe('When retrieving a random exercise', () => {
                    let response: Exercise | undefined;
                    beforeEach(() => {
                        response =
                            createWorkoutComponent.getRandomExercise(exercises);
                    });

                    it('should return an exercise', () => {
                        expect(response).toHaveProperty('bodyPart');
                    });
                });
            });

            describe('And equipment is selected to filter the exercises', () => {
                beforeEach(() => {
                    createWorkoutComponent.fullBodyWorkoutSelectedEquipment = [
                        'kettlebell',
                    ];
                });

                describe('When retrieving a random exercise', () => {
                    let response: Exercise | undefined;
                    beforeEach(() => {
                        response =
                            createWorkoutComponent.getRandomExercise(exercises);
                    });

                    it('should return an exercise with the selected equipment', () => {
                        expect(response?.equipment).toEqual('kettlebell');
                    });
                });
            });
        });
    });

    describe('Unit: setSelectedExerciseDetails', () => {
        describe('Given an exercise', () => {
            let mockExercise: Exercise;
            beforeEach(() => {
                mockExercise = {
                    bodyPart: 'bodyPart',
                    equipment: 'equipment',
                    gifUrl: 'gifUrl',
                    id: 'id',
                    name: 'name',
                    target: 'target',
                };
            });
            describe('When setting the selected exercise to view details for', () => {
                beforeEach(() => {
                    createWorkoutComponent.setSelectedExerciseDetails(
                        mockExercise
                    );
                });

                it('should set the selectedExerciseDetails', () => {
                    expect(
                        createWorkoutComponent.selectedExerciseDetails
                    ).toEqual(mockExercise);
                });
            });
        });

        describe('Given an undefined exercise', () => {
            let mockExercise: Exercise | undefined;
            beforeEach(() => {
                mockExercise = undefined;
            });

            describe('When setting the selected exercise to view details for', () => {
                beforeEach(() => {
                    createWorkoutComponent.setSelectedExerciseDetails(
                        mockExercise
                    );
                });

                it('should NOT set the selectedExerciseDetails', () => {
                    expect(
                        createWorkoutComponent.selectedExerciseDetails
                    ).toEqual({});
                });
            });
        });
    });

    describe('Unit: setModalTarget', () => {
        describe('Given an modal target', () => {
            const mockModalTarget = 'modalTarget';
            describe('When setting the modal target', () => {
                beforeEach(() => {
                    createWorkoutComponent.setModalTarget(mockModalTarget);
                });

                it('should set the selectedExerciseDetails', () => {
                    expect(createWorkoutComponent.modalTarget).toEqual(
                        mockModalTarget
                    );
                });
            });
        });
    });

    describe('Unit: saveWorkout', () => {
        describe('Given no workout name is set', () => {
            describe('When saving a workout', () => {
                beforeEach(() => {
                    jest.spyOn(window, 'alert').mockImplementation();
                    createWorkoutComponent.saveWorkout();
                });

                it('should alert that a workout name needs to be set', () => {
                    expect(window.alert).toHaveBeenCalledWith(
                        'Please enter a workout name!'
                    );
                });
            });
        });

        describe('And a workout name is set', () => {
            beforeEach(() => {
                createWorkoutComponent.workoutName = 'new workout';
            });

            describe('And the workout is saved successfully', () => {
                beforeEach(() => {
                    mockRequestService.saveWorkout.mockReturnValue(
                        of({
                            status: SaveWorkoutStatus.WORKOUT_SAVED,
                        })
                    );
                });

                describe('When saving a workout', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        createWorkoutComponent.saveWorkout();
                    });

                    it('should alert that the workout was saved successfully', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Workout saved successfully!'
                        );
                    });

                    it('should navigate to the home page', () => {
                        expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                            '/',
                        ]);
                    });
                });
            });

            describe('And a workout with the same name already exists', () => {
                beforeEach(() => {
                    mockRequestService.saveWorkout.mockReturnValue(
                        of({
                            status: SaveWorkoutStatus.WORKOUT_ALREADY_EXISTS,
                        })
                    );
                });

                describe('When saving a workout', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        createWorkoutComponent.saveWorkout();
                    });

                    it('should alert that a workout with the same name already exists', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Workout with this name already exists, please enter a new name.'
                        );
                    });
                });
            });

            describe('And the workout fails to save', () => {
                beforeEach(() => {
                    mockRequestService.saveWorkout.mockReturnValue(
                        of({
                            status: SaveWorkoutStatus.WORKOUT_SAVE_FAILED,
                        })
                    );
                });

                describe('When saving a workout', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        createWorkoutComponent.saveWorkout();
                    });

                    it('should alert that the workout failed to save', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Save workout failed. Please try again.'
                        );
                    });
                });
            });

            describe('And there is an error when saving the workout', () => {
                beforeEach(() => {
                    mockRequestService.saveWorkout.mockReturnValue(
                        throwError({ status: 404 })
                    );
                });

                describe('When saving a workout', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        jest.spyOn(console, 'log').mockImplementation();
                        createWorkoutComponent.saveWorkout();
                    });

                    it('should alert that the workout failed to save', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Save workout failed. Please try again.'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalled();
                    });
                });
            });
        });
    });
});
