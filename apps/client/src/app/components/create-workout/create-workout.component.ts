import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
    BodypartEnum,
    EquipmentEnum,
    SaveWorkoutStatus,
    TargetEnum,
} from '../../../../../models/enums';
import { Exercise } from '../../../../../models/exercise';
import { ExercisesService } from '../../services/exercises.service';
import { LoginService } from '../../services/login.service';
import { RequestService } from '../../services/request.service';

@Component({
    selector: 'simply-fit-create-workout',
    templateUrl: './create-workout.component.html',
    styleUrls: ['./create-workout.component.css'],
})
export class CreateWorkoutComponent implements OnInit {
    email = '';

    allExercises: Exercise[] = [];
    popularExercises: Exercise[] = [];
    selectedExercises: Exercise[] = [];
    savedExercises: Exercise[] = [];
    fullBodyWorkoutSelectedEquipment: string[] = [];
    filterText = '';
    equipmentEnum: EquipmentEnum[] = Object.values(EquipmentEnum);
    bodypartEnum: BodypartEnum[] = Object.values(BodypartEnum);
    targetEnum: TargetEnum[] = Object.values(TargetEnum);
    selectedEquipment = '';
    selectedBodypart = '';
    selectedTarget = '';
    selectedExerciseDetails: Exercise = {} as Exercise;
    modalTarget = '';
    workoutName = '';

    generatedWorkout = new Map<string, Exercise | undefined>();
    exercisesByBodyPart = new Map<string, Exercise[]>([
        ['back', []],
        ['chest', []],
        ['cardio', []],
        ['lower arms', []],
        ['upper arms', []],
        ['lower legs', []],
        ['upper legs', []],
        ['shoulders', []],
        ['neck', []],
        ['waist', []],
    ]);

    constructor(
        private exercisesService: ExercisesService,
        private requestService: RequestService,
        private loginService: LoginService,
        private router: Router
    ) {}

    async ngOnInit(): Promise<void> {
        this.loginService
            .getAccountEmail()
            .subscribe(async (accountEmail: string) => {
                this.email = accountEmail;
            });

        this.exercisesService
            .getAllExercises()
            .subscribe(async (exercises: Exercise[]) => {
                this.allExercises = exercises;

                this.popularExercises = await (
                    await this.requestService
                        .getPopularExercises(this.email)
                        .toPromise()
                ).exercises;

                this.allExercises.forEach((exercise: Exercise) => {
                    this.exercisesByBodyPart
                        .get(exercise.bodyPart)
                        ?.push(exercise);
                });

                this.popularExercises.forEach((exercise: Exercise) => {
                    this.exercisesByBodyPart
                        .get(exercise.bodyPart)
                        ?.push(exercise);
                });

                this.generateWorkout();
            });
    }

    selectExercise(exercise: Exercise | undefined) {
        if (exercise) {
            const itemIndex = this.selectedExercises.indexOf(exercise);
            if (itemIndex >= 0) {
                this.selectedExercises.splice(itemIndex, 1);
            } else {
                this.selectedExercises.push(exercise);
            }
        }
    }

    selectFullBodyWorkoutEquipment(equipment: string) {
        const itemIndex =
            this.fullBodyWorkoutSelectedEquipment.indexOf(equipment);
        if (itemIndex >= 0) {
            this.fullBodyWorkoutSelectedEquipment.splice(itemIndex, 1);
        } else {
            this.fullBodyWorkoutSelectedEquipment.push(equipment);
        }
    }

    saveChanges(): void {
        this.savedExercises = [...this.selectedExercises];
    }

    isChecked(exercise: Exercise | undefined): boolean {
        if (exercise) {
            const itemIndex = this.selectedExercises.indexOf(exercise);
            return itemIndex >= 0;
        }
        return false;
    }

    isSelectedEquipmentChecked(equipment: string): boolean {
        const itemIndex =
            this.fullBodyWorkoutSelectedEquipment.indexOf(equipment);
        return itemIndex >= 0;
    }

    resetFilters(): void {
        this.selectedEquipment = '';
        this.selectedBodypart = '';
        this.selectedTarget = '';
    }

    resetWorkout(): void {
        this.selectedExercises = [];
        this.savedExercises = [];
    }

    generateWorkout(): void {
        if (this.generatedWorkout.size > 0) {
            this.generatedWorkout = new Map<string, Exercise | undefined>();
        }

        this.generatedWorkout.set(
            'back',
            this.getRandomExercise(this.exercisesByBodyPart.get('back'))
        );
        this.generatedWorkout.set(
            'cardio',
            this.getRandomExercise(this.exercisesByBodyPart.get('cardio'))
        );
        this.generatedWorkout.set(
            'chest',
            this.getRandomExercise(this.exercisesByBodyPart.get('chest'))
        );
        this.generatedWorkout.set(
            'lower arms',
            this.getRandomExercise(this.exercisesByBodyPart.get('lower arms'))
        );
        this.generatedWorkout.set(
            'lower legs',
            this.getRandomExercise(this.exercisesByBodyPart.get('lower legs'))
        );
        this.generatedWorkout.set(
            'neck',
            this.getRandomExercise(this.exercisesByBodyPart.get('neck'))
        );
        this.generatedWorkout.set(
            'shoulders',
            this.getRandomExercise(this.exercisesByBodyPart.get('shoulders'))
        );
        this.generatedWorkout.set(
            'upper arms',
            this.getRandomExercise(this.exercisesByBodyPart.get('upper arms'))
        );
        this.generatedWorkout.set(
            'upper legs',
            this.getRandomExercise(this.exercisesByBodyPart.get('upper legs'))
        );
        this.generatedWorkout.set(
            'waist',
            this.getRandomExercise(this.exercisesByBodyPart.get('waist'))
        );
    }

    getRandomExercise(exercises: Exercise[] | undefined): Exercise | undefined {
        if (exercises) {
            const exercisesByEquipment = exercises.filter((exercise) => {
                if (this.fullBodyWorkoutSelectedEquipment.length > 0) {
                    return this.fullBodyWorkoutSelectedEquipment.includes(
                        exercise.equipment
                    );
                }
                return true;
            });

            return exercisesByEquipment[
                Math.floor(Math.random() * exercisesByEquipment.length)
            ];
        }
        return undefined;
    }

    setSelectedExerciseDetails(exercise: Exercise | undefined): void {
        if (exercise) {
            this.selectedExerciseDetails = exercise;
        }
    }

    setModalTarget(target: string): void {
        this.modalTarget = target;
    }

    saveWorkout(): void {
        if (this.workoutName) {
            this.requestService
                .saveWorkout(this.email, {
                    name: this.workoutName,
                    exercises: this.savedExercises,
                })
                .subscribe(
                    (response) => {
                        if (
                            response.status === SaveWorkoutStatus.WORKOUT_SAVED
                        ) {
                            alert('Workout saved successfully!');
                            this.router.navigate(['/']);
                        } else if (
                            response.status ===
                            SaveWorkoutStatus.WORKOUT_ALREADY_EXISTS
                        ) {
                            alert(
                                'Workout with this name already exists, please enter a new name.'
                            );
                        } else {
                            alert('Save workout failed. Please try again.');
                        }
                    },
                    (error) => {
                        alert('Save workout failed. Please try again.');
                        console.log(
                            `Save workout error: ${JSON.stringify(error)}`
                        );
                    }
                );
        } else {
            alert('Please enter a workout name!');
        }
    }
}
