import { Exercise } from './../../../../../models/exercise';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'simply-fit-exercise-display',
    templateUrl: './exercise-display.component.html',
    styleUrls: ['./exercise-display.component.css'],
})
export class ExerciseDisplayComponent {
    exercisesToDisplay: Exercise[];

    constructor(private router: Router) {
        this.exercisesToDisplay = history.state?.data;
        if (!this.exercisesToDisplay) {
            this.router.navigate(['/findExercises']);
        }
    }
}
