import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ExerciseDisplayComponent } from './exercise-display.component';

const mockRouter = {
    navigate: jest.fn(),
};

@Pipe({ name: 'safe' })
class MockPipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) {}

    transform(url: string): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}

describe('ExerciseDisplayComponent', () => {
    let exerciseDisplayComponent: ExerciseDisplayComponent;
    let fixture: ComponentFixture<ExerciseDisplayComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExerciseDisplayComponent, MockPipe],
            providers: [{ provide: Router, useValue: mockRouter }],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExerciseDisplayComponent);
        exerciseDisplayComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(exerciseDisplayComponent).toBeTruthy();
    });

    it('Should not have any exercises to display', () => {
        expect(exerciseDisplayComponent.exercisesToDisplay).toEqual(undefined);
    });

    describe('Given route history state data', () => {
        beforeEach(() => {
            history.pushState({ data: [{ exercise1: {}, exercise2: {} }] }, '');
        });

        describe('When the component is constructed', () => {
            beforeEach(() => {
                fixture = TestBed.createComponent(ExerciseDisplayComponent);
                exerciseDisplayComponent = fixture.componentInstance;
            });

            it('Should set the exercisesToDisplay', () => {
                expect(exerciseDisplayComponent.exercisesToDisplay).toEqual([
                    { exercise1: {}, exercise2: {} },
                ]);
            });
        });
    });

    describe('Given NO route history state data', () => {
        beforeEach(() => {
            history.pushState({}, '');
        });

        describe('When the component is constructed', () => {
            beforeEach(() => {
                fixture = TestBed.createComponent(ExerciseDisplayComponent);
                exerciseDisplayComponent = fixture.componentInstance;
            });

            it('Should navigate to find exercises', () => {
                expect(mockRouter.navigate).toHaveBeenCalledWith([
                    '/findExercises',
                ]);
            });
        });
    });
});
