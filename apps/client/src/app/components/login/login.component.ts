import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { RequestService } from '../../services/request.service';

@Component({
    selector: 'simply-fit-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent {
    constructor(
        private formBuilder: FormBuilder,
        private requestService: RequestService,
        private loginService: LoginService,
        private router: Router
    ) {}

    loginForm: FormGroup = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
    });

    get loginFormControl() {
        return this.loginForm.controls;
    }

    onSubmit(): void {
        if (this.loginForm.valid) {
            this.requestService.login(this.loginForm.value).subscribe(
                (response) => {
                    if (response.credentialsValid === true) {
                        this.loginService.logInAccount(this.loginForm.value);
                        alert('Logged in successfully!');
                        this.router.navigate(['/']);
                    } else {
                        alert('Log in failed. Please try again.');
                        this.loginForm.reset();
                    }
                },
                (error) => {
                    alert('Log in failed. Please try again.');
                    console.log(`Log in error: ${JSON.stringify(error)}`);
                    this.loginForm.reset();
                }
            );
        }
    }
}
