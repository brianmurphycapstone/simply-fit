import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    ReactiveFormsModule,
} from '@angular/forms';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { RequestService } from '../../services/request.service';

import { LoginComponent } from './login.component';

const mockRequestService = {
    login: jest.fn(),
};

const mockLoginService = {
    logInAccount: jest.fn(),
};

const mockRouter = {
    navigate: jest.fn(),
};

describe('LoginComponent', () => {
    let loginComponent: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [ReactiveFormsModule],
            declarations: [LoginComponent],
            providers: [
                FormBuilder,
                { provide: RequestService, useValue: mockRequestService },
                { provide: LoginService, useValue: mockLoginService },
                { provide: Router, useValue: mockRouter },
            ],
        }).compileComponents();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        loginComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(loginComponent).toBeTruthy();
    });

    describe('Unit: email validators', () => {
        describe('Given a FormGroup with email validators', () => {
            let control: AbstractControl;
            beforeEach(() => {
                control = loginComponent.loginForm.controls['email'];
            });

            describe('When validating an empty email', () => {
                beforeEach(() => {
                    control.setValue('');
                });

                it('Should indicate that email is required', () => {
                    expect(control.hasError('required')).toEqual(true);
                });
            });

            describe('When validating an incorrect email', () => {
                beforeEach(() => {
                    control.setValue('incorrect email');
                });

                it('Should indicate the email is not valid', () => {
                    expect(control.hasError('email')).toEqual(true);
                });
            });

            describe('When validating a valid email', () => {
                beforeEach(() => {
                    control.setValue('valid@email.com');
                });

                it('Should indicate the email is valid', () => {
                    expect(control.hasError('email')).toEqual(false);
                });
            });
        });
    });

    describe('Unit: password validators', () => {
        describe('Given a FormGroup with password validators', () => {
            let control: AbstractControl;
            beforeEach(() => {
                control = loginComponent.loginForm.controls['password'];
            });

            describe('When validating an empty password', () => {
                beforeEach(() => {
                    control.setValue('');
                });

                it('Should indicate that password is required', () => {
                    expect(control.hasError('required')).toEqual(true);
                });
            });
        });
    });

    describe('Unit: loginFormControl', () => {
        describe('Given a request to retrieve the loginFormControl', () => {
            let response: FormGroup['controls'];
            beforeEach(() => {
                response = loginComponent.loginFormControl;
            });

            it('should return the login form control', () => {
                expect(response).toEqual(loginComponent.loginFormControl);
            });
        });
    });

    describe('Unit: onSubmit', () => {
        describe('Given an invalid form', () => {
            describe('When calling onSubmit', () => {
                beforeEach(() => {
                    jest.spyOn(loginComponent.loginForm, 'reset');
                    loginComponent.onSubmit();
                });

                it('should NOT reset the form', () => {
                    expect(
                        loginComponent.loginForm.reset
                    ).not.toHaveBeenCalled();
                });
            });
        });
        describe('Given a valid form', () => {
            beforeEach(() => {
                const controls = loginComponent.loginForm.controls;
                for (const control in controls) {
                    controls[control].clearValidators();
                    controls[control].updateValueAndValidity({
                        onlySelf: true,
                    });
                }
                loginComponent.loginForm.updateValueAndValidity();
            });

            describe('And login is successful', () => {
                beforeEach(() => {
                    mockRequestService.login.mockReturnValue(
                        of({
                            credentialsValid: true,
                        })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        loginComponent.onSubmit();
                    });

                    it('should alert that login was successful', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Logged in successfully!'
                        );
                    });

                    it('should navigate to the home page', () => {
                        expect(mockRouter.navigate).toHaveBeenCalledWith(['/']);
                    });
                });
            });

            describe('And login fails', () => {
                beforeEach(() => {
                    mockRequestService.login.mockReturnValue(
                        of({
                            credentialsValid: false,
                        })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        jest.spyOn(loginComponent.loginForm, 'reset');
                        loginComponent.onSubmit();
                    });

                    it('should alert that login failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Log in failed. Please try again.'
                        );
                    });

                    it('should reset the form', () => {
                        expect(
                            loginComponent.loginForm.reset
                        ).toHaveBeenCalled();
                    });
                });
            });

            describe('And login returns an error', () => {
                beforeEach(() => {
                    mockRequestService.login.mockReturnValue(
                        throwError({ status: 404 })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        jest.spyOn(console, 'log').mockImplementation();
                        jest.spyOn(loginComponent.loginForm, 'reset');
                        loginComponent.onSubmit();
                    });

                    it('should alert that login failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Log in failed. Please try again.'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalled();
                    });

                    it('should reset the form', () => {
                        expect(
                            loginComponent.loginForm.reset
                        ).toHaveBeenCalled();
                    });
                });
            });
        });
    });
});
