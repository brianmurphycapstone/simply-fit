import { Workout } from '../../../../../models/workout';
import { RequestService } from '../../services/request.service';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Exercise } from '../../../../../models/exercise';
import { Router } from '@angular/router';

@Component({
    selector: 'simply-fit-track-workout',
    templateUrl: './view-workouts.component.html',
    styleUrls: ['./view-workouts.component.css'],
})
export class ViewWorkoutsComponent implements OnInit {
    email = '';
    savedWorkouts: Workout[] = [];
    selectedWorkout: Workout = {} as Workout;
    selectedExerciseDetails: Exercise = {} as Exercise;
    modalTarget = '';

    constructor(
        private requestService: RequestService,
        private loginService: LoginService,
        private router: Router
    ) {}

    async ngOnInit(): Promise<void> {
        this.loginService
            .getAccountEmail()
            .subscribe(async (accountEmail: string) => {
                this.email = accountEmail;
            });

        this.savedWorkouts = await (
            await this.requestService.getWorkouts(this.email).toPromise()
        ).workouts;
    }

    selectWorkout(workout: Workout): void {
        this.selectedWorkout = workout;
    }

    setSelectedExerciseDetails(exercise: Exercise | undefined): void {
        if (exercise) {
            this.selectedExerciseDetails = exercise;
        }
    }

    navigateToCreateWorkout() {
        this.router.navigate(['/createWorkout']);
    }
}
