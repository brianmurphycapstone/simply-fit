import { RequestService } from './../../services/request.service';
import { Router } from '@angular/router';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWorkoutsComponent } from './view-workouts.component';
import { LoginService } from '../../services/login.service';
import { of } from 'rxjs';
import { Workout } from '../../../../../models/workout';
import { Exercise } from '../../../../../models/exercise';

const mockRequestService = {
    getWorkouts: jest.fn().mockReturnValue(
        of({
            workouts: [],
        })
    ),
};

const mockRouter = {
    navigate: jest.fn(),
};

const mockLoginService = {
    getAccountEmail: jest
        .fn()
        .mockReturnValue(of({ email: 'test@test.com', password: '' })),
};

describe('TrackWorkoutComponent', () => {
    let viewWorkoutsComponent: ViewWorkoutsComponent;
    let fixture: ComponentFixture<ViewWorkoutsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ViewWorkoutsComponent],
            providers: [
                { provide: Router, useValue: mockRouter },
                { provide: LoginService, useValue: mockLoginService },
                { provide: RequestService, useValue: mockRequestService },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewWorkoutsComponent);
        viewWorkoutsComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(viewWorkoutsComponent).toBeTruthy();
    });

    describe('Unit: selectWorkout', () => {
        describe('Given a workout', () => {
            let mockWorkout: Workout;
            beforeEach(() => {
                mockWorkout = {
                    name: 'mockWorkouts',
                    exercises: [],
                };
            });

            describe('When selecting a workout to view', () => {
                beforeEach(() => {
                    viewWorkoutsComponent.selectWorkout(mockWorkout);
                });

                it('should set the selected workout', () => {
                    expect(viewWorkoutsComponent.selectedWorkout).toEqual(
                        mockWorkout
                    );
                });
            });
        });
    });

    describe('Unit: setSelectedExerciseDetails', () => {
        describe('Given an exercise', () => {
            let mockExercise: Exercise;
            beforeEach(() => {
                mockExercise = {
                    bodyPart: 'bodyPart',
                    equipment: 'equipment',
                    gifUrl: 'gifUrl',
                    id: 'id',
                    name: 'name',
                    target: 'target',
                };
            });
            describe('When setting the selected exercise to view details for', () => {
                beforeEach(() => {
                    viewWorkoutsComponent.setSelectedExerciseDetails(
                        mockExercise
                    );
                });

                it('should set the selectedExerciseDetails', () => {
                    expect(
                        viewWorkoutsComponent.selectedExerciseDetails
                    ).toEqual(mockExercise);
                });
            });
        });

        describe('Given an undefined exercise', () => {
            let mockExercise: Exercise | undefined;
            beforeEach(() => {
                mockExercise = undefined;
            });

            describe('When setting the selected exercise to view details for', () => {
                beforeEach(() => {
                    viewWorkoutsComponent.setSelectedExerciseDetails(
                        mockExercise
                    );
                });

                it('should NOT set the selectedExerciseDetails', () => {
                    expect(
                        viewWorkoutsComponent.selectedExerciseDetails
                    ).toEqual({});
                });
            });
        });
    });

    describe('Unit: navigateToCreateWorkout', () => {
        describe('When calling navigateToCreateWorkout', () => {
            beforeEach(() => {
                viewWorkoutsComponent.navigateToCreateWorkout();
            });

            it('should navigate to the create workout page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/createWorkout',
                ]);
            });
        });
    });
});
