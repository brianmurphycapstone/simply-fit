import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
    selector: 'simply-fit-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
    loggedIn = false;

    constructor(private router: Router, private loginService: LoginService) {}

    ngOnInit(): void {
        this.loginService.getLoggedInStatus().subscribe((status: boolean) => {
            this.loggedIn = status;
        });
    }

    navigateToRegister() {
        this.router.navigate(['/register']);
    }

    navigateToLogin() {
        this.router.navigate(['/login']);
    }

    navigateToFindExercises() {
        this.router.navigate(['/findExercises']);
    }

    navigateToCreateWorkout() {
        this.router.navigate(['/createWorkout']);
    }

    navigateToViewWorkouts() {
        this.router.navigate(['/viewWorkouts']);
    }
}
