import { Router } from '@angular/router';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent } from './landing-page.component';

const mockRouter = {
    navigate: jest.fn(),
};

describe('LandingPageComponent', () => {
    let landingPageComponent: LandingPageComponent;
    let fixture: ComponentFixture<LandingPageComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LandingPageComponent],
            providers: [{ provide: Router, useValue: mockRouter }],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LandingPageComponent);
        landingPageComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(landingPageComponent).toBeTruthy();
    });

    describe('Unit: navigateToRegister', () => {
        describe('When calling navigateToRetister', () => {
            beforeEach(() => {
                landingPageComponent.navigateToRegister();
            });

            it('should navigate to the register page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/register',
                ]);
            });
        });
    });

    describe('Unit: navigateToLogin', () => {
        describe('When calling navigateToLogin', () => {
            beforeEach(() => {
                landingPageComponent.navigateToLogin();
            });

            it('should navigate to the login page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/login',
                ]);
            });
        });
    });

    describe('Unit: navigateToFindExercises', () => {
        describe('When calling navigateToFindExercises', () => {
            beforeEach(() => {
                landingPageComponent.navigateToFindExercises();
            });

            it('should navigate to the find exercises page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/findExercises',
                ]);
            });
        });
    });

    describe('Unit: navigateToCreateWorkout', () => {
        describe('When calling navigateToCreateWorkout', () => {
            beforeEach(() => {
                landingPageComponent.navigateToCreateWorkout();
            });

            it('should navigate to the create workout page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/createWorkout',
                ]);
            });
        });
    });

    describe('Unit: navigateToViewWorkout', () => {
        describe('When calling navigateToViewWorkout', () => {
            beforeEach(() => {
                landingPageComponent.navigateToViewWorkouts();
            });

            it('should navigate to the view workouts page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/viewWorkouts',
                ]);
            });
        });
    });
});
