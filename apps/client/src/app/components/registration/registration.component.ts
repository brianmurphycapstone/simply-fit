import { Component } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators,
} from '@angular/forms';
import { RequestService } from '../../services/request.service';
import { AccountCreationStatus } from '../../../../../models/enums';
import { Router } from '@angular/router';

@Component({
    selector: 'simply-fit-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {
    constructor(
        private formBuilder: FormBuilder,
        private requestService: RequestService,
        private router: Router
    ) {}

    registrationForm: FormGroup = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: [
            '',
            [
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(20),
                this.passwordValidator(),
            ],
        ],
    });

    passwordValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!control.value) {
                return { invalidPassword: false };
            }
            const regex = new RegExp('^[a-zA-Z0-9]+$');
            const valid = regex.test(control.value);
            return valid ? {} : { invalidPassword: true };
        };
    }

    get registrationFormControl() {
        return this.registrationForm.controls;
    }

    onSubmit(): void {
        if (this.registrationForm.valid) {
            this.requestService
                .createAccount(this.registrationForm.value)
                .subscribe(
                    (response) => {
                        if (
                            response.status ===
                            AccountCreationStatus.ACCOUNT_CREATED
                        ) {
                            alert('Account created successfully!');
                            this.router.navigate(['login']);
                        } else if (
                            response.status ===
                            AccountCreationStatus.ACCOUNT_ALREADY_EXISTS
                        ) {
                            alert(
                                'Account already exists, please try a new email or log in with existing credentials.'
                            );
                        } else {
                            alert('Account creation failed. Please try again.');
                        }
                    },
                    (error) => {
                        alert('Account creation failed. Please try again.');
                        console.log(
                            `Account creation error: ${JSON.stringify(error)}`
                        );
                    }
                );
        }
        this.registrationForm.reset();
    }
}
