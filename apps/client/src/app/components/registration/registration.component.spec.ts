import {
    FormBuilder,
    AbstractControl,
    FormGroup,
    ReactiveFormsModule,
} from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationComponent } from './registration.component';
import { RequestService } from '../../services/request.service';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { AccountCreationStatus } from '../../../../../models/enums';

const mockRequestService = {
    createAccount: jest.fn(),
};

const mockRouter = {
    navigate: jest.fn(),
};

describe('RegistrationComponent', () => {
    let registrationComponent: RegistrationComponent;
    let fixture: ComponentFixture<RegistrationComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [ReactiveFormsModule],
            declarations: [RegistrationComponent],
            providers: [
                FormBuilder,
                { provide: RequestService, useValue: mockRequestService },
                { provide: Router, useValue: mockRouter },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RegistrationComponent);
        registrationComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(registrationComponent).toBeTruthy();
    });

    describe('Unit: email validators', () => {
        describe('Given a FormGroup with email validators', () => {
            let control: AbstractControl;
            beforeEach(() => {
                control =
                    registrationComponent.registrationForm.controls['email'];
            });

            describe('When validating an empty email', () => {
                beforeEach(() => {
                    control.setValue('');
                });

                it('Should indicate that email is required', () => {
                    expect(control.hasError('required')).toEqual(true);
                });
            });

            describe('When validating an incorrect email', () => {
                beforeEach(() => {
                    control.setValue('incorrect email');
                });

                it('Should indicate the email is not valid', () => {
                    expect(control.hasError('email')).toEqual(true);
                });
            });

            describe('When validating a valid email', () => {
                beforeEach(() => {
                    control.setValue('valid@email.com');
                });

                it('Should indicate the email is valid', () => {
                    expect(control.hasError('email')).toEqual(false);
                });
            });
        });
    });

    describe('Unit: password validators', () => {
        describe('Given a FormGroup with password validators', () => {
            let control: AbstractControl;
            beforeEach(() => {
                control =
                    registrationComponent.registrationForm.controls['password'];
            });

            describe('When validating an empty password', () => {
                beforeEach(() => {
                    control.setValue('');
                });

                it('Should indicate that password is required', () => {
                    expect(control.hasError('required')).toEqual(true);
                });
            });

            describe('When validating a password that is less than 8 characters', () => {
                beforeEach(() => {
                    control.setValue('1234567');
                });

                it('Should indicate that password is not valid', () => {
                    expect(control.hasError('minlength')).toEqual(true);
                });
            });

            describe('When validating a password that is between 8 and 20 characters', () => {
                beforeEach(() => {
                    control.setValue('password123');
                });

                it('Should indicate that password is valid', () => {
                    expect(control.hasError('minlength')).toEqual(false);
                    expect(control.hasError('maxlength')).toEqual(false);
                });
            });

            describe('When validating a password that is greater than 20 characters', () => {
                beforeEach(() => {
                    control.setValue('passwordthatistoolong');
                });

                it('Should indicate that password is NOT valid', () => {
                    expect(control.hasError('maxlength')).toEqual(true);
                });
            });

            describe('When validating a password that contains invalid symbols', () => {
                beforeEach(() => {
                    control.setValue('password!!!');
                });

                it('Should indicate that password is NOT valid', () => {
                    expect(control.hasError('invalidPassword')).toEqual(true);
                });
            });
        });
    });

    describe('Unit: registrationFormControl', () => {
        describe('Given a request to retrieve the registrationFormControl', () => {
            let response: FormGroup['controls'];
            beforeEach(() => {
                response = registrationComponent.registrationFormControl;
            });

            it('should return the registration form control', () => {
                expect(response).toEqual(
                    registrationComponent.registrationFormControl
                );
            });
        });
    });

    describe('Unit: onSubmit', () => {
        describe('Given an invalid form', () => {
            describe('When calling onSubmit', () => {
                beforeEach(() => {
                    jest.spyOn(registrationComponent.registrationForm, 'reset');
                    registrationComponent.onSubmit();
                });

                it('should reset the form', () => {
                    expect(
                        registrationComponent.registrationForm.reset
                    ).toHaveBeenCalled();
                });
            });
        });
        describe('Given a valid form', () => {
            beforeEach(() => {
                const controls =
                    registrationComponent.registrationForm.controls;
                for (const control in controls) {
                    controls[control].clearValidators();
                    controls[control].updateValueAndValidity({
                        onlySelf: true,
                    });
                }
                registrationComponent.registrationForm.updateValueAndValidity();
            });

            describe('And the account is created successfully', () => {
                beforeEach(() => {
                    mockRequestService.createAccount.mockReturnValue(
                        of({
                            status: AccountCreationStatus.ACCOUNT_CREATED,
                        })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        registrationComponent.onSubmit();
                    });

                    it('should alert that the account was created successfully', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Account created successfully!'
                        );
                    });

                    it('should navigate to the login page', () => {
                        expect(mockRouter.navigate).toHaveBeenCalledWith([
                            'login',
                        ]);
                    });
                });
            });

            describe('And the account already exists', () => {
                beforeEach(() => {
                    mockRequestService.createAccount.mockReturnValue(
                        of({
                            status: AccountCreationStatus.ACCOUNT_ALREADY_EXISTS,
                        })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        registrationComponent.onSubmit();
                    });

                    it('should alert that the account already exists', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Account already exists, please try a new email or log in with existing credentials.'
                        );
                    });
                });
            });

            describe('And the account creation fails', () => {
                beforeEach(() => {
                    mockRequestService.createAccount.mockReturnValue(
                        of({
                            status: AccountCreationStatus.ACCOUNT_CREATION_FAILED,
                        })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        registrationComponent.onSubmit();
                    });

                    it('should alert that the account creation failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Account creation failed. Please try again.'
                        );
                    });
                });
            });

            describe('And account creation returns an error', () => {
                beforeEach(() => {
                    mockRequestService.createAccount.mockReturnValue(
                        throwError({ status: 404 })
                    );
                });

                describe('When calling onSubmit', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        jest.spyOn(console, 'log').mockImplementation();
                        registrationComponent.onSubmit();
                    });

                    it('should alert that account creation failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Account creation failed. Please try again.'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalled();
                    });
                });
            });
        });
    });
});
