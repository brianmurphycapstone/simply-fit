import { LoginService } from '../../services/login.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FindExercisesComponent } from './find-exercises.component';
import { Router } from '@angular/router';
import { RequestService } from '../../services/request.service';
import { FormsModule } from '@angular/forms';
import { of, throwError } from 'rxjs';

const mockLoginService = {
    getLoggedInStatus: jest.fn().mockReturnValue(of(true)),
};

const mockRequestService = {
    getExercisesByEquipment: jest.fn(),
    getExercisesByBodypart: jest.fn(),
    getExercisesByTarget: jest.fn(),
};

const mockRouter = {
    navigate: jest.fn(),
};

jest.spyOn(console, 'log').mockImplementation();

describe('FindExercisesComponent', () => {
    let findExercisesComponent: FindExercisesComponent;
    let fixture: ComponentFixture<FindExercisesComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [FindExercisesComponent],
            providers: [
                { provide: LoginService, useValue: mockLoginService },
                { provide: RequestService, useValue: mockRequestService },
                { provide: Router, useValue: mockRouter },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FindExercisesComponent);
        findExercisesComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(findExercisesComponent).toBeTruthy();
    });

    it('should get the logged in status of the user', () => {
        expect(mockLoginService.getLoggedInStatus).toHaveBeenCalled();
        expect(findExercisesComponent.loggedIn).toEqual(true);
    });

    describe('Unit: findExercisesByEquipment', () => {
        describe('Given the user has NOT selected a piece of equipment to search for exercises', () => {
            describe('When the user searches for exercises', () => {
                beforeEach(() => {
                    jest.spyOn(window, 'alert').mockImplementation();
                    findExercisesComponent.findExercisesByEquipment();
                });

                it('should alert that you need to select equipment', () => {
                    expect(window.alert).toHaveBeenCalledWith(
                        'Please select equipment!'
                    );
                });
            });
        });

        describe('Given the user has selected a piece of equipment to search for exercises', () => {
            beforeEach(() => {
                findExercisesComponent.selectedEquipment = 'barbell';
            });

            describe('And the call to find exercises is successful', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByEquipment.mockReturnValue(
                        of({ exercises: [] })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        findExercisesComponent.findExercisesByEquipment();
                    });

                    it('should make a request to get exercises by equipment', () => {
                        expect(
                            mockRequestService.getExercisesByEquipment
                        ).toHaveBeenCalledWith('barbell');
                    });

                    it('should navigate to the exercise display view and pass the retrieved exercises', () => {
                        expect(mockRouter.navigate).toHaveBeenCalledWith(
                            ['/exerciseDisplay'],
                            { state: { data: [] } }
                        );
                    });
                });
            });

            describe('And the call to find exercises fails', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByEquipment.mockReturnValue(
                        throwError({ status: 404 })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        findExercisesComponent.findExercisesByEquipment();
                    });

                    it('should alert that the request failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Failed to find exercises, please try again!'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalledWith(
                            'Find by equipment error: {"status":404}'
                        );
                    });
                });
            });
        });
    });

    describe('Unit: findExercisesByBodypart', () => {
        describe('Given the user has NOT selected a bodypart to search for exercises', () => {
            describe('When the user searches for exercises', () => {
                beforeEach(() => {
                    jest.spyOn(window, 'alert').mockImplementation();
                    findExercisesComponent.findExercisesByBodypart();
                });

                it('should alert that you need to select a body part', () => {
                    expect(window.alert).toHaveBeenCalledWith(
                        'Please select body part!'
                    );
                });
            });
        });

        describe('Given the user has selected a bodypart to search for exercises', () => {
            beforeEach(() => {
                findExercisesComponent.selectedBodypart = 'back';
            });

            describe('And the call to find exercises is successful', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByBodypart.mockReturnValue(
                        of({ exercises: [] })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        findExercisesComponent.findExercisesByBodypart();
                    });

                    it('should make a request to get exercises by bodypart', () => {
                        expect(
                            mockRequestService.getExercisesByBodypart
                        ).toHaveBeenCalledWith('back');
                    });

                    it('should navigate to the exercise display view and pass the retrieved exercises', () => {
                        expect(mockRouter.navigate).toHaveBeenCalledWith(
                            ['/exerciseDisplay'],
                            { state: { data: [] } }
                        );
                    });
                });
            });

            describe('And the call to find exercises fails', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByBodypart.mockReturnValue(
                        throwError({ status: 404 })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        findExercisesComponent.findExercisesByBodypart();
                    });

                    it('should alert that the request failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Failed to find exercises, please try again!'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalledWith(
                            'Find by bodypart error: {"status":404}'
                        );
                    });
                });
            });
        });
    });

    describe('Unit: findExercisesByTarget', () => {
        describe('Given the user has NOT selected a target muscle to search for exercises', () => {
            describe('When the user searches for exercises', () => {
                beforeEach(() => {
                    jest.spyOn(window, 'alert').mockImplementation();
                    findExercisesComponent.findExercisesByTarget();
                });

                it('should alert that you need to select a target muscle', () => {
                    expect(window.alert).toHaveBeenCalledWith(
                        'Please select a target muscle!'
                    );
                });
            });
        });

        describe('Given the user has selected a target muscle to search for exercises', () => {
            beforeEach(() => {
                findExercisesComponent.selectedTarget = 'abs';
            });

            describe('And the call to find exercises is successful', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByTarget.mockReturnValue(
                        of({ exercises: [] })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        findExercisesComponent.findExercisesByTarget();
                    });

                    it('should make a request to get exercises by target muscle', () => {
                        expect(
                            mockRequestService.getExercisesByTarget
                        ).toHaveBeenCalledWith('abs');
                    });

                    it('should navigate to the exercise display view and pass the retrieved exercises', () => {
                        expect(mockRouter.navigate).toHaveBeenCalledWith(
                            ['/exerciseDisplay'],
                            { state: { data: [] } }
                        );
                    });
                });
            });

            describe('And the call to find exercises fails', () => {
                beforeEach(() => {
                    mockRequestService.getExercisesByTarget.mockReturnValue(
                        throwError({ status: 500 })
                    );
                });
                describe('When the user searches for exercises', () => {
                    beforeEach(() => {
                        jest.spyOn(window, 'alert').mockImplementation();
                        findExercisesComponent.findExercisesByTarget();
                    });

                    it('should alert that the request failed', () => {
                        expect(window.alert).toHaveBeenCalledWith(
                            'Failed to find exercises, please try again!'
                        );
                    });

                    it('should log the error', () => {
                        expect(console.log).toHaveBeenCalledWith(
                            'Find by target error: {"status":500}'
                        );
                    });
                });
            });
        });
    });
});
