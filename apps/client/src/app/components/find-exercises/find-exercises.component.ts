import { TargetEnum } from './../../../../../models/enums';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BodypartEnum, EquipmentEnum } from '../../../../../models/enums';
import { ExerciseResponse } from '../../../../../models/exercise';
import { LoginService } from '../../services/login.service';
import { RequestService } from '../../services/request.service';

@Component({
    selector: 'simply-fit-find-exercises',
    templateUrl: './find-exercises.component.html',
    styleUrls: ['./find-exercises.component.css'],
})
export class FindExercisesComponent implements OnInit {
    equipmentEnum: EquipmentEnum[] = Object.values(EquipmentEnum);
    bodypartEnum: BodypartEnum[] = Object.values(BodypartEnum);
    targetEnum: TargetEnum[] = Object.values(TargetEnum);
    selectedEquipment = '';
    selectedBodypart = '';
    selectedTarget = '';
    loggedIn = false;

    constructor(
        private loginService: LoginService,
        private requestService: RequestService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.loginService.getLoggedInStatus().subscribe((status: boolean) => {
            this.loggedIn = status;
        });
    }

    findExercisesByEquipment() {
        if (this.selectedEquipment) {
            this.requestService
                .getExercisesByEquipment(this.selectedEquipment)
                .subscribe(
                    (response: ExerciseResponse) => {
                        this.router.navigate(['/exerciseDisplay'], {
                            state: { data: response.exercises },
                        });
                    },
                    (error) => {
                        alert('Failed to find exercises, please try again!');
                        console.log(
                            `Find by equipment error: ${JSON.stringify(error)}`
                        );
                    }
                );
        } else {
            alert('Please select equipment!');
        }
    }

    findExercisesByBodypart() {
        if (this.selectedBodypart) {
            this.requestService
                .getExercisesByBodypart(this.selectedBodypart)
                .subscribe(
                    (response: ExerciseResponse) => {
                        this.router.navigate(['/exerciseDisplay'], {
                            state: { data: response.exercises },
                        });
                    },
                    (error) => {
                        alert('Failed to find exercises, please try again!');
                        console.log(
                            `Find by bodypart error: ${JSON.stringify(error)}`
                        );
                    }
                );
        } else {
            alert('Please select body part!');
        }
    }

    findExercisesByTarget() {
        if (this.selectedTarget) {
            this.requestService
                .getExercisesByTarget(this.selectedTarget)
                .subscribe(
                    (response: ExerciseResponse) => {
                        this.router.navigate(['/exerciseDisplay'], {
                            state: { data: response.exercises },
                        });
                    },
                    (error) => {
                        alert('Failed to find exercises, please try again!');
                        console.log(
                            `Find by target error: ${JSON.stringify(error)}`
                        );
                    }
                );
        } else {
            alert('Please select a target muscle!');
        }
    }
}
