import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'simply-fit-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
    loggedIn = false;
    email = '';

    constructor(private loginService: LoginService, private router: Router) {}

    ngOnInit(): void {
        this.loginService.getLoggedInStatus().subscribe((status: boolean) => {
            this.loggedIn = status;
        });
        this.loginService
            .getAccountEmail()
            .subscribe((accountEmail: string) => {
                this.email = accountEmail;
            });
    }

    navigateToHome() {
        this.router.navigate(['/']);
    }

    navigateToFindExercises() {
        this.router.navigate(['/findExercises']);
    }

    navigateToLogin() {
        this.router.navigate(['/login']);
    }

    navigateToCreateWorkout() {
        this.router.navigate(['/createWorkout']);
    }

    navigateToViewWorkouts() {
        this.router.navigate(['/viewWorkouts']);
    }

    logOut() {
        this.router.navigate(['/']);
        this.loginService.logOut();
    }
}
