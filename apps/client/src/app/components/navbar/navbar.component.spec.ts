import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { LoginService } from '../../services/login.service';

import { NavbarComponent } from './navbar.component';

const mockLoginService = {
    getLoggedInStatus: jest.fn().mockReturnValue(of(true)),
    getAccountEmail: jest.fn().mockReturnValue(of('test@test.com')),
    logOut: jest.fn(),
};

const mockRouter = {
    navigate: jest.fn(),
};

describe('NavbarComponent', () => {
    let navbarComponent: NavbarComponent;
    let fixture: ComponentFixture<NavbarComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [NavbarComponent],
            providers: [
                { provide: LoginService, useValue: mockLoginService },
                { provide: Router, useValue: mockRouter },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NavbarComponent);
        navbarComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(navbarComponent).toBeTruthy();
    });

    it('should set the logged in status', () => {
        expect(mockLoginService.getLoggedInStatus).toHaveBeenCalled();
        expect(navbarComponent.loggedIn).toEqual(true);
    });

    it('should set the email', () => {
        expect(mockLoginService.getAccountEmail).toHaveBeenCalled();
        expect(navbarComponent.email).toEqual('test@test.com');
    });

    describe('Unit: navigateToHome', () => {
        describe('When calling navigateToHome', () => {
            beforeEach(() => {
                navbarComponent.navigateToHome();
            });

            it('should navigate to the landing page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith(['/']);
            });
        });
    });

    describe('Unit: navigateToFindExercises', () => {
        describe('When calling navigateToFindExercises', () => {
            beforeEach(() => {
                navbarComponent.navigateToFindExercises();
            });

            it('should navigate to the find exercises page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/findExercises',
                ]);
            });
        });
    });

    describe('Unit: navigateToLogin', () => {
        describe('When calling navigateToLogin', () => {
            beforeEach(() => {
                navbarComponent.navigateToLogin();
            });

            it('should navigate to the login page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/login',
                ]);
            });
        });
    });

    describe('Unit: navigateToCreateWorkout', () => {
        describe('When calling navigateToCreateWorkout', () => {
            beforeEach(() => {
                navbarComponent.navigateToCreateWorkout();
            });

            it('should navigate to the create workout page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/createWorkout',
                ]);
            });
        });
    });

    describe('Unit: navigateToViewWorkouts', () => {
        describe('When calling navigateToViewWorkouts', () => {
            beforeEach(() => {
                navbarComponent.navigateToViewWorkouts();
            });

            it('should navigate to the view workouts page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith([
                    '/viewWorkouts',
                ]);
            });
        });
    });

    describe('Unit: logout', () => {
        describe('When calling logout', () => {
            beforeEach(() => {
                navbarComponent.logOut();
            });

            it('should navigate to the home page', () => {
                expect(mockRouter.navigate).toHaveBeenLastCalledWith(['/']);
            });

            it('should log the user out', () => {
                expect(mockLoginService.logOut).toHaveBeenCalled();
            });
        });
    });
});
