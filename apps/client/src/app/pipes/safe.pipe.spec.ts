import { SafeResourceUrl } from '@angular/platform-browser';
import { SafePipe } from './safe.pipe';

const mockDomSanitizer = {
    bypassSecurityTrustResourceUrl: jest.fn(),
    sanitize: jest.fn(),
    bypassSecurityTrustHtml: jest.fn(),
    bypassSecurityTrustStyle: jest.fn(),
    bypassSecurityTrustScript: jest.fn(),
    bypassSecurityTrustUrl: jest.fn(),
};
describe('SafePipe', () => {
    let pipe: SafePipe;
    it('create an instance', () => {
        pipe = new SafePipe(mockDomSanitizer);
        expect(pipe).toBeTruthy();
    });

    describe('Unit: transform', () => {
        describe('When transforming a url', () => {
            let response: SafeResourceUrl;

            beforeEach(() => {
                mockDomSanitizer.bypassSecurityTrustResourceUrl.mockReturnValue(
                    'sanitized.test.com'
                );
                response = pipe.transform('test.com');
            });

            it('should call the sanitizer on the url', () => {
                expect(
                    mockDomSanitizer.bypassSecurityTrustResourceUrl
                ).toHaveBeenCalledWith('test.com');
            });

            it('should return the sanitized url', () => {
                expect(response).toEqual('sanitized.test.com');
            });
        });
    });
});
