import { Pipe, PipeTransform } from '@angular/core';
import { Exercise } from '../../../../models/exercise';

@Pipe({
    name: 'filterExercises',
})
export class FilterExercisesPipe implements PipeTransform {
    transform(
        exercises: Exercise[],
        filterText: string,
        selectedBodypart: string,
        selectedEquipment: string,
        selectedTarget: string
    ): Exercise[] {
        return exercises.filter((exercise) => {
            let bodyPartMatch = true;
            let equipmentMatch = true;
            let targetMatch = true;

            const textMatch = exercise.name.includes(
                filterText.toLocaleLowerCase()
            );

            if (selectedBodypart) {
                bodyPartMatch = exercise.bodyPart === selectedBodypart;
            }

            if (selectedEquipment) {
                equipmentMatch = exercise.equipment === selectedEquipment;
            }

            if (selectedTarget) {
                targetMatch = exercise.target === selectedTarget;
            }

            return textMatch && bodyPartMatch && equipmentMatch && targetMatch;
        });
    }
}
