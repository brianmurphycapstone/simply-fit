import { Exercise } from '../../../../models/exercise';
import { FilterExercisesPipe } from './filter-exercises.pipe';

describe('FilterExercisesPipe', () => {
    let pipe: FilterExercisesPipe;
    it('create an instance', () => {
        pipe = new FilterExercisesPipe();
        expect(pipe).toBeTruthy();
    });

    describe('Unit: transform', () => {
        describe('Given a list of exercises', () => {
            let exercises: Exercise[];

            beforeEach(() => {
                exercises = [
                    {
                        name: 'exercise1',
                        bodyPart: 'body',
                        equipment: 'equipment',
                        target: 'target',
                    } as Exercise,
                    { name: 'different name' } as Exercise,
                ];
            });

            describe('And filter text, selected body part, selected equipment and selected target', () => {
                let filterText: string;
                let selectedBodypart: string;
                let selectedTarget: string;
                let selectedEquipment: string;

                beforeEach(() => {
                    filterText = 'exer';
                    selectedBodypart = 'body';
                    selectedTarget = 'target';
                    selectedEquipment = 'equipment';
                });

                describe('When filtering exercises based on filter text', () => {
                    let response: Exercise[];

                    beforeEach(() => {
                        response = pipe.transform(
                            exercises,
                            filterText,
                            selectedBodypart,
                            selectedEquipment,
                            selectedTarget
                        );
                    });

                    it('should filter the exercises based on the filters', () => {
                        expect(response).toEqual([
                            {
                                name: 'exercise1',
                                bodyPart: 'body',
                                equipment: 'equipment',
                                target: 'target',
                            } as Exercise,
                        ]);
                    });
                });
            });

            describe('And NO filters', () => {
                let filterText: string;
                let selectedBodypart: string;
                let selectedTarget: string;
                let selectedEquipment: string;

                beforeEach(() => {
                    filterText = '';
                    selectedBodypart = '';
                    selectedTarget = '';
                    selectedEquipment = '';
                });

                describe('When filtering exercises based on filter text', () => {
                    let response: Exercise[];

                    beforeEach(() => {
                        response = pipe.transform(
                            exercises,
                            filterText,
                            selectedBodypart,
                            selectedTarget,
                            selectedEquipment
                        );
                    });

                    it('should return the original list of exercises', () => {
                        expect(response).toEqual([
                            {
                                name: 'exercise1',
                                bodyPart: 'body',
                                equipment: 'equipment',
                                target: 'target',
                            } as Exercise,
                            { name: 'different name' } as Exercise,
                        ]);
                    });
                });
            });
        });
    });
});
