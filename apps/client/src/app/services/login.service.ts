import { Account } from './../../../../models/account';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class LoginService {
    private loggedInStatus$: Observable<boolean>;
    private _loggedInStatus$: BehaviorSubject<boolean>;
    private accountEmail$: Observable<string>;
    private _accountEmail$: BehaviorSubject<string>;

    constructor() {
        this._accountEmail$ = new BehaviorSubject<string>('');
        this.accountEmail$ = this._accountEmail$.asObservable();
        this._loggedInStatus$ = new BehaviorSubject<boolean>(false);
        this.loggedInStatus$ = this._loggedInStatus$.asObservable();

        const loggedInAccount = this.getCookie('loggedInAccount');
        if (loggedInAccount) {
            this.logInAccount({ email: loggedInAccount, password: '' });
        }
    }

    getAccountEmail(): Observable<string> {
        return this.accountEmail$;
    }

    logInAccount(account: Account): void {
        this._accountEmail$.next(account.email);
        this._loggedInStatus$.next(true);
    }

    logOut(): void {
        this._accountEmail$.next('');
        this._loggedInStatus$.next(false);
        document.cookie = 'loggedInAccount= ';
    }

    getLoggedInStatus(): Observable<boolean> {
        return this.loggedInStatus$;
    }

    getCookie(cName: string) {
        const name = cName + '=';
        const cDecoded = decodeURIComponent(document.cookie);
        const cArr = cDecoded.split('; ');
        let res;
        cArr.forEach((val) => {
            if (val.indexOf(name) === 0) res = val.substring(name.length);
        });
        return res;
    }
}
