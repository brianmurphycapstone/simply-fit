import { GetWorkoutResponse } from './../../../../models/workout';
import { ExerciseResponse } from './../../../../models/exercise';
import { LoginResponse } from './../../../../models/login';
import { AccountCreationResponse } from '../../../../models/account';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Account } from '../../../../models/account';
import { SaveWorkoutResponse, Workout } from '../../../../models/workout';

@Injectable({
    providedIn: 'root',
})
export class RequestService {
    constructor(private http: HttpClient) {}

    createAccount(account: Account) {
        return this.http.post<AccountCreationResponse>(
            '/api/createAccount',
            account
        );
    }

    login(account: Account) {
        return this.http.post<LoginResponse>('/api/login', account);
    }

    getExercisesByEquipment(equipment: string) {
        return this.http.get<ExerciseResponse>(
            '/api/exercises/equipment/' + equipment
        );
    }

    getExercisesByBodypart(bodypart: string) {
        return this.http.get<ExerciseResponse>(
            '/api/exercises/bodypart/' + bodypart
        );
    }

    getExercisesByTarget(target: string) {
        return this.http.get<ExerciseResponse>(
            '/api/exercises/target/' + target
        );
    }

    getAllExercises() {
        return this.http.get<ExerciseResponse>('/api/exercises/');
    }

    saveWorkout(email: string, workout: Workout) {
        return this.http.post<SaveWorkoutResponse>('/api/saveWorkout/', {
            email: email,
            workout: workout,
        });
    }

    getWorkouts(email: string) {
        return this.http.post<GetWorkoutResponse>('/api/getWorkouts/', {
            email: email,
        });
    }

    getPopularExercises(email: string) {
        return this.http.post<ExerciseResponse>('/api/exercises/popular/', {
            email: email,
        });
    }
}
