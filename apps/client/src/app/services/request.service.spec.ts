import { SaveWorkoutStatus } from './../../../../models/enums';
import { ExerciseResponse } from './../../../../models/exercise';
import { LoginResponse } from './../../../../models/login';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { AccountCreationStatus } from '../../../../models/enums';
import { AccountCreationResponse, Account } from '../../../../models/account';
import { RequestService } from './request.service';
import { of } from 'rxjs';
import {
    GetWorkoutResponse,
    SaveWorkoutResponse,
    Workout,
} from '../../../../models/workout';

const mockHttpClient = {
    post: jest.fn(),
    get: jest.fn(),
};

describe('RequestService', () => {
    let requestService: RequestService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: HttpClient, useValue: mockHttpClient }],
        });
        requestService = TestBed.inject(RequestService);
    });

    it('should be created', () => {
        expect(requestService).toBeTruthy();
    });

    describe('Unit: createAccount', () => {
        describe('When requesting to create an account', () => {
            let response: AccountCreationResponse;
            let account: Account;
            beforeEach(async () => {
                account = {
                    email: 'email',
                    password: 'password',
                };
                mockHttpClient.post.mockReturnValue(
                    of({
                        status: AccountCreationStatus.ACCOUNT_CREATED,
                    })
                );
                response = await requestService
                    .createAccount(account)
                    .toPromise();
            });

            it('Should call the create account endpoint', () => {
                expect(mockHttpClient.post).toHaveBeenCalledWith(
                    '/api/createAccount',
                    account
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    status: AccountCreationStatus.ACCOUNT_CREATED,
                });
            });
        });
    });

    describe('Unit: login', () => {
        describe('When requesting to create an account', () => {
            let response: LoginResponse;
            let account: Account;
            beforeEach(async () => {
                account = {
                    email: 'email',
                    password: 'password',
                };
                mockHttpClient.post.mockReturnValue(
                    of({
                        credentialsValid: true,
                    })
                );
                response = await requestService.login(account).toPromise();
            });

            it('Should call the login endpoint', () => {
                expect(mockHttpClient.post).toHaveBeenCalledWith(
                    '/api/login',
                    account
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    credentialsValid: true,
                });
            });
        });
    });

    describe('Unit: getExercisesByEquipment', () => {
        describe('When requesting to get exercises by equipment', () => {
            let response: ExerciseResponse;
            beforeEach(async () => {
                mockHttpClient.get.mockReturnValue(
                    of({
                        exercises: [],
                    })
                );
                response = await requestService
                    .getExercisesByEquipment('barbell')
                    .toPromise();
            });

            it('Should call the exercise by equipment endpoint', () => {
                expect(mockHttpClient.get).toHaveBeenCalledWith(
                    '/api/exercises/equipment/barbell'
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    exercises: [],
                });
            });
        });
    });

    describe('Unit: getExercisesByBodypart', () => {
        describe('When requesting to get exercises by bodypart', () => {
            let response: ExerciseResponse;
            beforeEach(async () => {
                mockHttpClient.get.mockReturnValue(
                    of({
                        exercises: [],
                    })
                );
                response = await requestService
                    .getExercisesByBodypart('back')
                    .toPromise();
            });

            it('Should call the exercise by bodypart endpoint', () => {
                expect(mockHttpClient.get).toHaveBeenCalledWith(
                    '/api/exercises/bodypart/back'
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    exercises: [],
                });
            });
        });
    });

    describe('Unit: getExercisesByTarget', () => {
        describe('When requesting to get exercises by target muscle', () => {
            let response: ExerciseResponse;
            beforeEach(async () => {
                mockHttpClient.get.mockReturnValue(
                    of({
                        exercises: [],
                    })
                );
                response = await requestService
                    .getExercisesByTarget('abs')
                    .toPromise();
            });

            it('Should call the exercise by target endpoint', () => {
                expect(mockHttpClient.get).toHaveBeenCalledWith(
                    '/api/exercises/target/abs'
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    exercises: [],
                });
            });
        });
    });

    describe('Unit: saveWorkout', () => {
        describe('When requesting to save a workout', () => {
            let response: SaveWorkoutResponse;
            let workout: Workout;
            const email = 'email@email.com';

            beforeEach(async () => {
                workout = {
                    name: 'workoutName',
                    exercises: [],
                };
                mockHttpClient.post.mockReturnValue(
                    of({
                        status: SaveWorkoutStatus.WORKOUT_SAVED,
                    })
                );
                response = await requestService
                    .saveWorkout(email, workout)
                    .toPromise();
            });

            it('Should call the save workout endpoint', () => {
                expect(mockHttpClient.post).toHaveBeenCalledWith(
                    '/api/saveWorkout/',
                    {
                        email: email,
                        workout: workout,
                    }
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    status: SaveWorkoutStatus.WORKOUT_SAVED,
                });
            });
        });
    });

    describe('Unit: getWorkouts', () => {
        describe('When requesting to get workouts', () => {
            let response: GetWorkoutResponse;
            const email = 'email@email.com';

            beforeEach(async () => {
                mockHttpClient.post.mockReturnValue(
                    of({
                        workouts: [],
                    })
                );
                response = await requestService.getWorkouts(email).toPromise();
            });

            it('Should call the get workouts endpoint', () => {
                expect(mockHttpClient.post).toHaveBeenCalledWith(
                    '/api/getWorkouts/',
                    { email: email }
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    workouts: [],
                });
            });
        });
    });

    describe('Unit: getPopularExercises', () => {
        describe('When requesting to get popular exercises', () => {
            let response: ExerciseResponse;
            const email = 'email@email.com';

            beforeEach(async () => {
                mockHttpClient.post.mockReturnValue(
                    of({
                        exercises: [],
                    })
                );
                response = await requestService
                    .getPopularExercises(email)
                    .toPromise();
            });

            it('Should call the get popular exercises endpoint', () => {
                expect(mockHttpClient.post).toHaveBeenCalledWith(
                    '/api/exercises/popular/',
                    { email: email }
                );
            });

            it('Should return a response', () => {
                expect(response).toEqual({
                    exercises: [],
                });
            });
        });
    });
});
