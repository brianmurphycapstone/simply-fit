import { RequestService } from './request.service';
import { Injectable } from '@angular/core';
import { Exercise } from '../../../../models/exercise';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ExercisesService {
    private exercises$: Observable<Exercise[]>;
    private _exercises$: ReplaySubject<Exercise[]>;

    constructor(private requestService: RequestService) {
        this._exercises$ = new ReplaySubject<Exercise[]>();
        this.exercises$ = this._exercises$.asObservable();
        requestService.getAllExercises().subscribe((response) => {
            this._exercises$.next(response.exercises);
        });
    }

    getAllExercises(): Observable<Exercise[]> {
        return this.exercises$;
    }
}
