import { TestBed } from '@angular/core/testing';
import { Exercise } from '../../../../models/exercise';
import { of } from 'rxjs';
import { ExercisesService } from './exercises.service';
import { RequestService } from './request.service';

const mockRequestService = {
    getAllExercises: jest
        .fn()
        .mockReturnValue(
            of({ exercises: [{ name: 'exercise1' } as Exercise] })
        ),
};
describe('ExercisesService', () => {
    let exercisesService: ExercisesService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                { provide: RequestService, useValue: mockRequestService },
            ],
        });
        exercisesService = TestBed.inject(ExercisesService);
    });

    it('should be created', () => {
        expect(exercisesService).toBeTruthy();
    });

    describe('Unit: getAllExercises', () => {
        describe('When retrieving the all exercises', () => {
            it('should return the list of exercises', (done) => {
                exercisesService
                    .getAllExercises()
                    .subscribe((result: Exercise[]) => {
                        expect(result).toEqual([
                            { name: 'exercise1' } as Exercise,
                        ]);
                        done();
                    });
            });
        });
    });
});
