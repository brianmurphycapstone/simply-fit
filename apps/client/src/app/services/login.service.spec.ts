import { TestBed } from '@angular/core/testing';
import { LoginService } from './login.service';

describe('LoginService', () => {
    let loginService: LoginService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        loginService = TestBed.inject(LoginService);
    });

    it('should be created', () => {
        expect(loginService).toBeTruthy();
    });

    describe('Unit: getAccount', () => {
        describe('When retrieving the account', () => {
            it('Should return the account', (done) => {
                loginService
                    .getAccountEmail()
                    .subscribe((accountEmail: string) => {
                        expect(accountEmail).toEqual('');
                        done();
                    });
            });
        });
    });

    describe('Unit: getLoggedInStatus', () => {
        describe('When retrieving the logged in status', () => {
            it('Should return the logged in status', (done) => {
                loginService
                    .getLoggedInStatus()
                    .subscribe((status: boolean) => {
                        expect(status).toEqual(false);
                        done();
                    });
            });
        });
    });

    describe('Unit: logInAccount', () => {
        describe('When logging in an account', () => {
            beforeEach(() => {
                loginService.logInAccount({
                    email: 'email',
                    password: 'password',
                });
            });
            it('Should update the accout information', (done) => {
                loginService
                    .getAccountEmail()
                    .subscribe((accountEmail: string) => {
                        expect(accountEmail).toEqual('email');
                        done();
                    });
            });

            it('Should set the logged in status to true', (done) => {
                loginService
                    .getLoggedInStatus()
                    .subscribe((status: boolean) => {
                        expect(status).toEqual(true);
                        done();
                    });
            });
        });
    });

    describe('Unit: logOut', () => {
        describe('When logging out', () => {
            beforeEach(() => {
                loginService.logOut();
            });
            it('Should update the accout information', (done) => {
                loginService
                    .getAccountEmail()
                    .subscribe((accountEmail: string) => {
                        expect(accountEmail).toEqual('');
                        done();
                    });
            });

            it('Should set the logged in status to false', (done) => {
                loginService
                    .getLoggedInStatus()
                    .subscribe((status: boolean) => {
                        expect(status).toEqual(false);
                        done();
                    });
            });
        });
    });

    describe('Given a user is logged in and their email is saved as a cookie', () => {
        beforeEach(() => {
            document.cookie = 'loggedInAccount=test@test.com;';
        });

        describe('When creating the login service', () => {
            let loginService2: LoginService;
            beforeEach(() => {
                loginService2 = new LoginService();
            });

            it('should set the logged in email', (done) => {
                loginService2
                    .getAccountEmail()
                    .subscribe((accountEmail: string) => {
                        expect(accountEmail).toEqual('test@test.com');
                        done();
                    });
            });
        });
    });
});
