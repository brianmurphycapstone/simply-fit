import { ViewWorkoutsComponent } from './components/view-workouts/view-workouts.component';
import { CreateWorkoutComponent } from './components/create-workout/create-workout.component';
import { FindExercisesComponent } from './components/find-exercises/find-exercises.component';
import { ExerciseDisplayComponent } from './components/exercise-display/exercise-display.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { RegistrationComponent } from './components/registration/registration.component';

const routes: Routes = [
    { path: '', component: LandingPageComponent },
    { path: 'register', component: RegistrationComponent },
    { path: 'login', component: LoginComponent },
    { path: 'findExercises', component: FindExercisesComponent },
    { path: 'exerciseDisplay', component: ExerciseDisplayComponent },
    { path: 'createWorkout', component: CreateWorkoutComponent },
    { path: 'viewWorkouts', component: ViewWorkoutsComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
