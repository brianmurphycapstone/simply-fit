import { ExercisesService } from './services/exercises.service';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExerciseDisplayComponent } from './components/exercise-display/exercise-display.component';
import { FindExercisesComponent } from './components/find-exercises/find-exercises.component';
import { SafePipe } from './pipes/safe.pipe';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CreateWorkoutComponent } from './components/create-workout/create-workout.component';
import { FilterExercisesPipe } from './pipes/filter-exercises.pipe';
import { ViewWorkoutsComponent } from './components/view-workouts/view-workouts.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        CommonModule,
        MatButtonModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    declarations: [
        AppComponent,
        LandingPageComponent,
        RegistrationComponent,
        LoginComponent,
        ExerciseDisplayComponent,
        FindExercisesComponent,
        SafePipe,
        NavbarComponent,
        CreateWorkoutComponent,
        FilterExercisesPipe,
        ViewWorkoutsComponent,
    ],
    providers: [ExercisesService],
    exports: [AppComponent],
    bootstrap: [AppComponent],
})
export class AppModule {}
