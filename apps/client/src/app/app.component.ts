import { Component } from '@angular/core';
import { ExercisesService } from './services/exercises.service';

@Component({
    selector: 'simply-fit-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    constructor(private exercisesService: ExercisesService) {}
}
